\section{Identifying trends in SWoT processes}
\label{sec:analysis}

The landscape of the relationship between semantic Fog computing and \gls{swot} architectures depicted in the previous section in Section \textsection \ref{subs:analysis} some insights on the trends observed in the surveyed publications. 
In the process of the survey, we identified some functions which seemed relevant to the evolution of the \gls{swot} domain towards semantic Fog computing, but which were under-represented in the surveyed publications.
It did not allow us to make generic statements as part of the previous survey, but these functions are presented in Section \textsection \ref{subs:future_functions} as perspectives for future developments of the \gls{swot}.
The survey is focused on the integration of \gls{sw} technologies in \gls{iot} architectures and Fog nodes, but there are also interesting evolutions of the \gls{sw} in order to adapt to \gls{swot} requirements.
The purpose of Section \textsection \ref{subs:sw_transformation} is to give an overview of these evolutions.
Finally, a perspective on the complementarity of semantic Cloud and Fog computing in supporting \gls{swot} deployment is provided in Section \textsection \ref{subs:fog_transparency}.

\subsection{Analysis of the surveyed contributions}
\label{subs:analysis}

\begin{figure}
	\centering
	%\raggedleft
	\caption{Summary of identified functions, distributed per percentage of studies based on semantic Fog computing}
	\label{fig:processes_summary}
	\scalebox{0.8}{
		\input{035_survey/figures/functions_summary.pgf}
	}
\end{figure}

An overview of Tab. \ref{fig:processes_summary}, and the analysis conducted in Section \textsection \ref{sec:contributions}, leads to an initial answer to the research question: Fog nodes indeed actively support the deployment of \gls{swot} functions. 
However, the simplicity of this answer does not cover the complexity of the relation between the Fog computing and \gls{swot} domains.

Situating \gls{swot} contributions in a reference 3-layered architecture shows how \textbf{nodes constrain the functions they support}: contributions dedicated to the same function but at different levels do not expect similar outcomes. 
This observation is an explanation for the predominance of solutions based on semantic Cloud computing in resource-intensive functions such as aggregation. 
On the other hand, functions that can be implemented with simple mechanisms requiring few resources, such as discovery or exposition, are easier to distribute on constrained nodes. 

The notion of context scale is also a determining factor. 
Functions such as enrichment can be performed at a local scale, in a limited context, which is easy to achieve in Fog architectures, by definition more context-specific than Cloud architectures. 
Since Fog computing is based on highly distributed nodes, it is adapted to parallelizable tasks, in which a local context is sufficient.
On the other hand, functions such as presentation or composition are based in the surveyed contribution on a global context, centralized on a Cloud node. 
Aside from the resource point of view, distributing such global context among Fog nodes could introduce delays and consistency issues.

The identified functions have been defined as atomic units that can be combined together into functions of higher order. 
That is why papers introducing full platforms, such as \cite{Kibria2015}, \cite{Poslad2015}, \cite{Perera2016} or \cite{Le-Phuoc2016} are related to numerous functions: complex functionalities are achieved by building sequences of simpler functions.
\analyticsPapers papers share a common high-level function, namely analytics, a function where content is used to support decision-making and prediction. 
Decision support has two aspects: on the one hand, the supported decision-making process can be performed by human beings, and on the other hand it applies to autonomic systems. 
The former case is not specific to the \gls{iot}, but can be instantiated with a sensor network, and the system only provides guidance to a human decision maker.
Analytics is implemented in \cite{Fensel2017}, \cite{Pease2017}, \cite{Howell2017}, \cite{Wang2018} as a mix of content abstraction and aggregation, completed with presentation.
Past content is aggregated with various mathematical approaches, and background knowledge is used in combination with aggregated content in order to achieve prediction, which is a form of abstraction.
Both aggregated content and predictions are presented to the user in the form of graphs, maps, alerts, or dashboards, helping a human operator to capture a global understanding of the context.
Some smart city scenarios use sensor networks and \gls{iot} deployments for decision support, \textit{e.g.,} Dublin's Smart Energy Demand Analysis\footnote{\url{http://smartdublin.ie/smartstories/spatial-energy-demand-analysis/}}.
In this survey, all \analyticsPapers papers implementing analytics does so on Cloud nodes. 
It is consistent with the considerable computing resources required to perform such a function, as well as with the globality of the contextualization of content preferred in decision-making that is better adapted to semantic Cloud computing.
\cite{Ullah2017} provides a good illustration of the motivation for deploying analytics functionalities on Cloud nodes: the context necessary for a successful analysis is very large, and does not match the locality of decision-making enabled by Fog computing.
\cite{Ploennigs2017} also considers analytics on Cloud nodes, but implements other functions (\textit{e.g.,} enrichment) with semantic Fog computing, thus combining the characteristics of Fog and Cloud computing for complementary functions.

When composing functions to implement complex applications, a notion of lifecycle can be defined, where functions are chained in an order depending on the proposed application, and depend on each other. 
In this lifecycle, functions are executed either on Fog nodes or on Cloud nodes. 
The location of an individual function can also be explained by its dependency to other functions. 
For instance, if the enrichment of content is based on node abstractions that are only available on Cloud nodes, it is more likely that the enrichment function will also be implemented by semantic Cloud computing, even if the resources it requires would be adapted to a deployment among Fog nodes. 
These dependencies can be found in approaches promoting composition such as \cite{Vlacheas2013} or \cite{Foteinos2013}: the composition function is based on node abstractions built in the Cloud. 
These dependencies also explain the predominance of enrichment and node abstraction functions. 
Most of the identified functions depend on semantically described nodes and content, and enrichment or abstraction are prerequisite to advanced \gls{swot} deployments.
The increase of contributions based on semantic-aware Fog nodes for these preliminary functions in recent years enabled the upward trend of semantic Fog computing for functions such as content abstraction or node discovery.
Of course, lifecycles can also be directed from the Cloud tier to the Fog tier of the Cloud-Fog-Device pattern as well, and complex deductions produced by Cloud nodes can be disseminated among Fog nodes.

\subsection{Envisioning future functions}
\label{subs:future_functions}
When proposing functions to describe contributions brought in the surveyed papers, we also identified functional units that provided functionalities relevant to \gls{swot} deployments, but to the best of our knowledge they were not instantiated in the papers selected in the survey. 
The development of these functions is a next step in the evolution of the \gls{swot} domain, as captured in \cite{Wang2015}.
The authors describe the initial notion of sensor devices, which evolved gradually into
\begin{itemize}
	\item Sensor networks with the development of communications,
	\item Sensor Web with the concern of interactivity, accessibility and formats,
	\item \gls{wot} with the integration of smart Things
	\item finally Semantic sensor networks, fusion of the Semantic Web with Sensor Web.
\end{itemize}
The \gls{swot} we depict is to the \gls{wot} what the Semantic sensor network is to the Sensor Web.
The integration of actuators, and the possibility of communicating back and forth with constrained devices (instead of only consuming the data they collect), leads to the necessity for lowering, described in Section \textsection \ref{par:lowering}.
The growing development of the \gls{swot} also leads to the emergence of datasets, devices networks and applications using different ontologies, potentially to describe similar domains. 
Translating content from one ontology to the other is therefore an important interoperability-enabling function, that is only implemented in \translationPapers contribution. 
We emphasize its potential in Section \textsection \ref{subsubs:translation}.
Transforming content from one ontology to the other also raises consistency issues.
Even more generally, consistency of the world representation in a dynamic \gls{swot} system is an important issue, discussed in Section \textsection \ref{subsubs:consistency}
Finally, the increase of publications basing basic functions on semantic Fog computing allows to expect the shift to semantic-enabled Fog nodes for some other functions, especially composition, as explained in Section \textsection \ref{par:fog_composition}.

\subsubsection{Lowering}
\label{par:lowering}

Lowering is the function exactly \textbf{opposed to enrichment}: it is the transformation of content from a semantically rich format to a less expressive, more constrained representation. 
It is a content value creation function of the DI transformation category.
The need for such transformation arose from the integration not only of sensors, but also of actuators in the \gls{iot}. 
These devices are content consumers, but their constrained nature prevents them from being able to consume some types of content. 
Actions represented in rich formats in Cloud or Fog nodes need to be adapted to the target device so that despite their constraints, they can interpret these actions correctly and behave as intended. 
This transformation deprives the content of part of its expressivity and context, but the transformed, simpler content is interpreted in a known context, leading to a \textbf{trade-off for consistency}. 
Lowering requires the source node to have a representation of the capabilities and expectations of the remote target node.
The source node in this function is by design more powerful than the destination node, and that is why the former should be aware of the restrictions of the latter. 
Lowering should not be confused with work such as \cite{Maarala2017}, \cite{Su2018} or \cite{Charpenay2018}, in which new formats are proposed to be supported in constrained environments. 
Even though these contributions are very valuable to the development of the \gls{swot}, they \textit{de facto} exclude legacy devices, that have no semantic capabilities whatsoever. 
The purpose of lowering is to produce low-level data, by taking advantage of the contextual interpretation performed by the legacy device.

Lowering is opposed in the literature to "lifting", a synonym for enrichment. 
\cite{Kopecky2007} describes SAWDSL, a language aiming at making it possible to lift XML to RDF and to lower RDF to XML thanks to XML schema annotations. 
The mapping reversal approach \cite{Seydoux2016_poster}, introduced in Section \textsection \ref{sec:mapping_reversal}, is an example of automatic lowering intended to support autonomic reasoning.

Overall, the transformation of content from a high-level representation to a form that can be processed by a constrained node is still a challenge for the \gls{swot}. 
The lack of interest in this function partly comes from its contradiction with the usual practices in the \gls{sw} community. 
Content is generally moved upward in the \gls{dikw} pyramid, because it gains value this way. 
However, the presence of \textbf{constrained content consumers} (actuator nodes) on the \gls{iot}, combined to the need for high-level content in more powerful nodes, makes lowering a function as necessary as enrichment for a complete \gls{swot} deployment.

\subsubsection{Translation}
\label{subsubs:translation}

Translation is a function in which content enriched using an ontology is described with another one. 
It is a content value creation function of the \gls{di} transformation category.
Translation enables interoperability between deployments based on different vocabularies.
There are too few contributions to the translation function for us to make general observations as part of the survey.

\cite{Howell2017} is the only paper fitting the inclusion criteria in which translation is discussed. 
The authors refer to it as schema conversion, and implement such function in order to promote interoperability from their own domain ontology to SAREF, a reference smart building ontology. 
Mappings are expressed explicitly in the source vocabulary, and they are used in SPARQL CONSTRUCT queries to build an equivalent graph in the target vocabulary.
This approach is deployed in a Cloud-based platform.

The ontologies of the \gls{swot} are diverse, and only a few are reused by other \cite{Seydoux2016}: the vertical fracturing between domains is not bridged, since domain-specific concepts are redefined disconnected from each other. 
Among the surveyed papers, SSN is the only widely reused ontology.
In order to bring these ontologies together, alignments can be added, and the use of high-level ontologies (such as DUL\footnote{\url{http://www.ontologydesignpatterns.org/ont/dul/DUL.owl}}) or reference ontologies eases the integration of different ontologies in the same knowledge system. 
Due to the dynamicity of \gls{iot}, static manual alignments are not sufficient for a full semantic interoperability.
A first contribution to translation in the \gls{swot} was proposed by \cite{Kotis2012a} and \cite{Kotis2012}, that are not included in the survey because the nature of the proposed deployment is unclear. 
The authors however underline the importance of automating entity mapping to allow the dynamic deployment of heterogeneous devices.
\cite{Shvaiko2013} lists ontology alignment tools, and identifies challenges, some of which are relevant to support the translation function by semantic-aware Fog nodes, such as matching efficiency or collaborative approaches. 
An example of such collaborative approach is provided by \cite{Santos2016}, where correspondences are discovered through local exchange between nodes.
This kind of automatic alignment technique is suited to a deployment among Fog nodes, even if not initially directed at the \gls{swot}.

\subsubsection{Consistency enforcement}
\label{subsubs:consistency}

Consistency enforcement is a content validity check function.
It is a content value creation function of the IK transformation category.
The notion of validity covers both the absence of contradictions among the facts expressed in the content, and the absence of wrong assertions (\textit{e.g.,} sensor measures different from the reality of the physical world). 

The only surveyed paper implementing this function is \cite{Kibria2015}, where the consistency of inferred knowledge is validated using a reasoner. 
Both the inference and consistency enforcement are based on semantic Cloud computing. 
Other papers, such as \cite{Charpenay2015}, discuss consistency enforcement techniques on \gls{swot} content, but no details about the deployment are provided by the authors.

Consistency is not instantiated in detail in the contributions we surveyed, even if \cite{Corcho2010} identifies consistency as a challenge for the \gls{swot}.
Contradictions between several nodes can be dealt with in an aggregation function where data fusion techniques are applied, but consistency enforcement also includes the detection of logical issues in models and their instantiations. 
More generally, consistency enforcement is not a topic limited to the \gls{swot}, and \textbf{generic consistency mechanisms could be applied to \gls{iot} datasets}.
The integration of translation should be linked to a generalization of consistency enforcement: when manipulating different models, some local mappings might induce global inconsistencies that the system should be able to detect and prevent.

\subsubsection{Composition} 
\label{par:fog_composition}

Increasing the availability of node abstractions and computing resources within Fog architectures gives potential to dynamically compose nodes in order to provide different services.
The techniques that were initially developed for semantic Cloud computing that are presented in Section \textsection \ref{par:composition} could be adapted to semantic Fog computing, and techniques for service composition natively suitable to semantic-enabled Fog nodes such as \cite{Martini2015} could be adapted to the \gls{swot}.
Composing nodes in the Fog tier enriches local context, and exposes richer service to a Cloud node without requiring it to compute compositions, providing more scalability to large deployments.


\subsection{Adapting Semantic Web technologies to constraints of the IoT domain}
\label{subs:sw_transformation}

The issues tackled by emergence of the \gls{swot} are issues affecting the development of the \gls{iot} and the deployment of functions within \gls{iot} architectures. 
These issues (\textit{e.g.,} heterogeneity, lack of interoperability, content transformation) are recurrent concerns for the \gls{sw} community, not necessarily related to the \gls{iot} or the \gls{wot}. 
However, the \gls{iot} also has intrinsic characteristics due to the constraints on its constituting nodes, the distributed nature of its deployments, and the dynamism of its topology. 
These constraints apply to any solution deployed in an \gls{iot} architecture. 
That is why \textbf{contributions between the \gls{sw} and the \gls{iot} domains are not unidirectional}.
The \gls{sw} does contribute to the emergence of the \gls{swot} domain by providing interoperability solutions to the \gls{iot} domain, but \gls{sw} technologies and principles must also be adapted to meet \gls{iot} constraints in order to develop \gls{swot} architectures.

One of the most important constraints in the \gls{iot} is the presence of physical nodes with limited capabilities, for both low-level devices and Fog nodes. 
The constraints on these nodes were presented in Section \textsection \ref{subs:node_classes}. 
Among the resources that can be limited for a node, we distinguished energy, processing power, communication channels, and memory. 
These resources are not independent, \textit{e.g.,} the limitation of processing power or of time exchanging messages over the communication channel saves energy. 
In order to be suitable for an \gls{iot} network, a solution should dynamically adapt to the node running it, and the integration of \gls{sw} principles and technologies must be thought differently on each level of the deployment architecture: a device does not have the capability to run the full \gls{sw} stack. 
The sheer emergence of the functions identified in the literature is a first adaptation to node constraints. 
Functions are spread across the \gls{iot} depending of the nodes able to support it. 
That is why resource-intensive functions such as analytics are performed by Cloud nodes, and simpler functions such as enrichment are deployed among Fog nodes. 

Energy is a primary concern in the \gls{iot} domain for two reasons. 
On the one hand, the dissemination of nodes in the environment makes it hard to connect them to a power grid (especially in non-urban areas, such as fields or forests). 
Therefore, some nodes run on batteries, and their lifetime is directly related to their consumption of energy. 
On the other hand, the multiplication of nodes implies a multiplication of power consumers, and managing energy consumption on the scale of the node leads to energy saving on a more global scale. 
Using \gls{sw} technologies to reduce the cost of content transport was already discussed in the description of the associated functions presented by \cite{Ashraf2010}. 
However, in this particular paper, \gls{sw} technologies are not modified in order to comply with the constraints of an \gls{iot} network. 

To show how \gls{sw} technologies can be adapted to \gls{iot} network requirements, \cite{Su2015} compares the different serialization formats available for semantically rich data with respect to the size of the messages encoded in each format, as well as the number of CPU cycles required to produce these messages.  
The energy consumption associated to the creation, the transmission, the reception and the decoding of these messages is then compared for each format. 
In this paper, \textbf{two aspects of energy consumption} are pointed out: the \textbf{processing} required to handle the content, and the \textbf{communication} channels required to exchange it.

Reducing the cost of the communication can be achieved by using protocols adapted to the needs and constraints of the \gls{iot}, and by adapting existing platforms to these protocols. 
For instance, \cite{Loseto2016} proposes an extension of the \gls{ldp}\footnote{\url{https://www.w3.org/TR/ldp/}} specification, a recommendation of the W3C. 
The W3C natively maps primitives of \gls{ldp} to HTTP, and the authors of this paper propose a new mapping of \gls{ldp} to CoAP, in order to make it suitable for constrained applications. 
CoAP is a protocol especially designed for constrained applications, with reduced headers and limited packet body, which has already been introduced in Chapter \textsection \ref{chap:background}.
Such an initiative allows \gls{iot} nodes to be connected to the \gls{lod}, and therefore to extend the \gls{wot}, while respecting the constraints of \gls{iot} nodes.

Distributing content via adapted protocol also requires said content to be stored on the device distributing it. 
Contributions such as \cite{Bazoobandi2015} or \cite{Charpenay2018} aim at allowing devices with limited memory to store semantically rich data. In the former, the authors propose a method to store compressed RDF data in memory while ensuring a certain level of efficiency regarding encoding and decoding of data. 
This paper is not specifically targeted to the \gls{iot} domain, but its contribution matches the requirements of this domain. 
Similarly, \cite{Woensel2018} proposes an algorithm for reasoning in memory-constrained environments which is not solely dedicated to the \gls{swot} domain, but supports its development.
The ability to be able to access and modify the stored data efficiently is important in the case of streaming data, the authors point out.
In the case of \gls{iot} deployments, and especially of sensor networks, the ability of storing and distributing up-to-date data is an important feature, and there must be a trade-off between memory optimization, and cost of encoding/decoding.
\cite{Hasemann2012} also proposes a tuple store suitable for embedded systems, as well as a protocol-independent RDF broker, that can be mapped to CoAP for instance. 
The authors propose the adoption of a protocol stack adapted to constrained nodes in order to include them into the \gls{swot} without the need for smart gateways acting as proxies.

\subsection{Making semantic Fog computing transparent}
\label{subs:fog_transparency}

With its IaaS, Paas, and SaaS services \cite{Colombo-Mendoza2012}, the Cloud computing approach is based on easing access to resources by making the underlying complexity transparent. 
Cloud-based applications are widely popular nowadays, and Cloud providers such as AWS\footnote{\url{https://aws.amazon.com}}, Google Cloud\footnote{\url{https://cloud.google.com/}} or Microsoft Azure\footnote{\url{https://azure.microsoft.com}} offer transparent, instantaneous access to Cloud resources through APIs.
Academic work has been pursued on Cloud architectures, to enable service orchestration \cite{Bousselmi2014} or application bursting \cite{Charrada2016} for instance, taking advantage of the simplicity of available resources. 

\gls{iot} applications also benefitted from the availability of large resources transparently accessible.
\gls{iot} solutions targeted to end-users such as Apple Homekit\footnote{\url{https://www.apple.com/ios/home/}} or Orange Homelive\footnote{\url{http://homelive.particule.orange.fr/}} are examples of \gls{iot} platforms transparently relying on Cloud nodes.
Data collected from the local context of the user is stored and processed on third-party Cloud servers, providing a Web interface accessible from anywhere to the user.
Privacy issues are not considered here, but rather user experience.

In its Cloud-based form supported by semantic-agnostic Fog nodes, the \gls{swot} can be deployed in such Cloud architectures.
However, we saw that deploying \gls{swot} functionalities among semantic-enabled Fog nodes fostered scalability and reduction of application response time by bringing computation closer to data producers and consumers.
If observation data is computed in an autonomic process to take action on local devices, performing the computation on a remote server induces a round trip from the local Fog node to Cloud nodes and back, leading to what is called a ``trombone effect''\footnote{\url{https://www.networkworld.com/article/2224213/cisco-subnet/why-the--trombone--effect-is-problematic-for-enterprise-internet-access.html}}, or ``tromboning''.

Considering that the \textbf{transparency of the infrastructure underlying the Cloud architecture} is a key element of its adoption, a next step in the integration of semantic-enabled Fog nodes in the \gls{swot} architectural pattern is to \textbf{make semantic Fog computing transparent} as well.
\cite{Patel2017} identifies gaps in the current technologies constituting hurdles to the deployment of \gls{swot} systems among Fog nodes.
The heterogeneity of the Fog architecture is one of them, which provides motivation to try reusing the experience and competencies gathered in the Cloud computing community to hide the underlying Fog architecture complexity.
Similarly to Cloud infrastructure, building affordable orchestration mechanisms for Fog infrastructure would support its active role in the deployment of the \gls{swot}.

The transparency of semantic Fog computing is also a convergence point for a Cloud-Fog collaboration: rather than being seen as opposed, the Cloud and Fog paradigms should be seen as complementary approaches, each having specific characteristics and constraints.
Cloud computing already provides an abstraction layer to cluster physical machines, therefore it is possible to envision the Cloud architecture as a wrapper for Fog nodes.
In such a collaborative deployment, applications could communicate with Cloud nodes, where resource-intensive computations are located, along with large data storage. 
Part of the computation could also be distributed and processed by Fog computing, in order to be brought closer to \gls{iot} devices, in a dynamic architecture that is hidden from the end user.
Such an approach would enable the deployment the \gls{swot} technological stack while taking advantage of both Cloud and Fog paradigm characteristics.
% Content processes
\newcommand{\enrichmentPapers}{28\xspace}
\newcommand{\enrichmentFogPapers}{15\xspace}
\newcommand{\enrichmentFogRate}{54\xspace}
\newcommand{\translationPapers}{1\xspace}
\newcommand{\contentAbstractionPapers}{20\xspace}
\newcommand{\contentAbstractionFogPapers}{9\xspace}
\newcommand{\analyticsPapers}{9\xspace}
\newcommand{\presentationPapers}{10\xspace}
\newcommand{\nodeAbstractionPapers}{30\xspace}
\newcommand{\compositionPapers}{9\xspace}
\newcommand{\discoverypapers}{11\xspace}
\newcommand{\discoveryFogPapers}{8\xspace}
\newcommand{\expositionPapers}{10\xspace}
\newcommand{\expositionFogPapers}{7\xspace}
\newcommand{\selectionPapers}{10\xspace}
\newcommand{\selectionCloudPapers}{7\xspace}
\newcommand{\accessPapers}{21\xspace}
\newcommand{\accessFogPapers}{11\xspace}
\newcommand{\disseminationPapers}{8\xspace}
\newcommand{\disseminationFogPapers}{5\xspace}
\newcommand{\surveyedPapers}{64\xspace}
\newcommand{\fogActiveRate}{58\xspace}

\chapter[Semantic Cloud and Fog computing for the SWoT]{Supporting the SWoT with semantic Cloud and Fog computing: a survey}
\minitoc
\label{chap:survey}

Achieving semantic interoperability is a primary issue in the \gls{iot}, that leads to the emergence of the \gls{swot}.
However, the integration of \gls{sw} technologies into \gls{iot} systems is quite challenging due to some characteristics and constraints of the \gls{iot} domain.
While \gls{iot} architectures are by design based on \textbf{resource-constrained devices}, the \gls{sw} technologies are \textbf{resource-consuming}. 
In order to tackle this core divergence, it is obvious that \gls{sw} technologies cannot be deployed directly on \gls{iot} devices in the majority of cases.
Therefore, \textbf{the \gls{sw} stack has to be instantiated along the reference \gls{swot} three-tier architectural pattern} characterized in Section \textsection \ref{subs:three-tier_archi}.

The Cloud computing paradigm is well-established, and Cloud architectures support a majority of Web services used at a world-wide scale.
However, as it has been described in Section \textsection \ref{subs:cloud_fog_device}, some characteristics that are desirable for \gls{swot} deployments are not compatible with the nature of Cloud architectures. 
In order to complement said Cloud architectures by instantiating these properties, the Fog computing paradigm has been proposed. 
Fog computing focuses on localizing processing power closer to \gls{iot} devices in distributed architecture, and its impact on the \gls{swot} has been studied for instance in \cite{Patel2017}.

This chapter aims at studying \textbf{the role of Fog computing in supporting the deployment of the \gls{swot}} by surveying the literature.
\textbf{The term ``Fog computing'' has been coined rather recently} \cite{Bonomi2012} compared to ``Cloud computing'' (1994\footnote{https://www.wired.com/1994/04/general-magic/}), ``\gls{iot}'' (1999 according to \cite{Ashton2009}) and ``Semantic Web'' (\cite{Berners-Lee2001}). 
However, with the vision of Fog nodes as the equipment connecting \gls{iot} devices to Cloud servers provided in Section \textsection \ref{subs:cloud_fog_device}, we characterized Fog nodes as an intrinsic component of the \gls{swot} architectural pattern discussed in Section \textsection \ref{subs:three-tier_archi}.
In this Cloud-Fog-Device pattern, the role of Fog nodes is double: 
\begin{itemize}
	\item On the one hand, Fog nodes provide technical interoperability between Cloud nodes and devices. 
	In this case, Fog nodes serve as gateways connecting \gls{wot} and \gls{iot} tiers, and do not implement any element of the \gls{sw} stack, which is deployed on Cloud nodes.
	We refer to this deployment type as \textbf{semantic-agnostic Fog nodes}, and the processing based on \gls{sw} technologies is qualified as \textbf{semantic Cloud computing}.
	\item On the other hand, the processing power provided by Fog computing may be used to support some elements of the \gls{sw} stack.
	Fog nodes still provide communication capabilities between the devices and Cloud nodes, but they are also full-fledged components of the \gls{swot} deployment.
	We refer to this kind of approach as based on \textbf{semantic-enabled Fog nodes}, enabling \textbf{semantic Fog computing}.
\end{itemize}
This distinction entails a question: how is the use of either semantic-enabled or semantic-agnostic Fog nodes impacting the \gls{swot} domain ?

The purpose of this survey chapter is to provide a reading grid to:
\begin{enumerate*}
	\item classify the research contributions transforming the \gls{iot} into the \gls{swot} domain, and
	\item analyze the integration of semantic Fog computing in this evolution by comparing it with semantic Cloud computing.
\end{enumerate*}

The ambivalent role of Fog nodes in \gls{swot} architectures is used to define precisely the research question addressed by the ensuing survey with a description of the systematic literature review methodology followed.
Recurrent patterns in existing research are identified, and used in conjunction with the reference architecture to describe the role of Fog computing in the integration of the \gls{sw} principles into \gls{iot} networks in Section \textsection \ref{sec:contributions}. 
In Section \textsection \ref{sec:analysis},deductions are extracted from the survey, then we discuss future directions for the \gls{swot}. 
Challenges the \gls{sw} faces to be compliant with constraints of the \gls{iot} domain are pointed out, as well as recent contributions from the \gls{sw} research community proposed to adapt to these constraints. 
The complementarity of the Cloud and Fog paradigms in supporting the \gls{swot} architecture is brought into focus.

\section{Survey context and key terms}
\label{sec:fog_iot}

In order to discuss the relationship between Fog computing and the implementation of the \gls{swot} technological stack, this section describes the context of the conducted survey. 
The literature review methodology followed is described in Section \textsection \ref{subs:survey_methodo}, and the elements of the reading grid used to classify contributions are presented in Section \textsection \ref{subs:swot_functions}.
Related surveys are presented in Section \textsection \ref{subs:related_work}.

\subsection{Survey methodology}
\label{subs:survey_methodo}

This survey has been conducted according to the systematic literature search technique presented in \cite{Kitchenham2004}.
Since we established that Fog nodes are part of the \gls{swot} architectural pattern in Section \textsection \ref{subs:three-tier_archi}, the question is whether:
\begin{itemize}
	\item \gls{swot} technologies are only deployed in Cloud nodes, with semantic-agnostic Fog nodes being solely technical and syntactic interoperability providers, or
	\item if semantic-aware Fog nodes play an active role in the implementation of the \gls{swot} technical stack to provide semantic interoperability.
\end{itemize}
We want to determine how semantic Fog computing supports \gls{swot} architectures in the case where Fog nodes are semantically enabled, and the motivations for preferring semantic Cloud computing otherwise.

To answer this question, papers were collected from major digital libraries (namely IEEEXplore, ScienceDirect and ACM Digital Library) with the disjunction of keywords ``SWoT'', ``Semantic Web'', and ``Internet of Things''. 
Both journal and conference papers were considered. 
Moreover, in order to focus on papers relevant to the research question, only papers in which the deployment architecture is clearly described were considered.
Such a description can be done in a figure, by providing hardware capabilities of the deployment architecture, or by describing the capabilities of the deployment nodes.
Contributions based on a purely functional architectures were excluded from the survey if the components were not situated on identified containers (machines classifiable as Cloud or Fog nodes).
Since not all studies relevant to our survey explicitly referred to the equipment between \gls{iot} devices and remote Cloud servers as ``Fog'' nodes, but also as ``gateways'' for instance, the keywords ``Fog'' and ``Cloud'' were not used as filters.
The relevance of architectures has been considered based on the characteristics described in the surveyed studies.
No minimal date was considered, and the latest papers included were prior to July, 2018.
Finally, \surveyedPapers publications were considered for the survey, sorted by year of publication in Fig. \ref{fig:paper_per_year}. 

\begin{figure}
	\centering
	\caption{Surveyed papers, per year}
	\label{fig:paper_per_year}
	\input{035_survey/figures/papers_per_year.pgf}
\end{figure}

Fig. \ref{fig:paper_per_year} shows an increase of the number of papers relevant to our survey with time, with a proportional increase of the papers in which Fog nodes play an active role in \gls{swot} functions. 
Overall, semantic capabilities are distributed on Fog nodes in \fogActiveRate\% of the studied papers.
It should be noted that this rate is biased by the inclusion criteria for papers selected in this study: by default, papers where Fog nodes do not play an active role for semantic processing, and where the \gls{sw} technological stack is only deployed in Cloud nodes do not provide details about their deployment architecture. 
The choice to exclude such papers from the survey was made because their analysis would have been entirely based on an assumption that is directly correlated with the research question we want to discuss. 
Therefore, the rate of papers both contributing to the \gls{swot} and considering an active Fog node over all papers dedicated to the \gls{swot} in general is likely to be much lower than it is in this survey.
% XXX Faire un topo sur la représentation du terme "Fog" dans le papier
The purpose is rather to explore the role of Fog nodes in \gls{swot} architectures rather than give a precise estimation of how many papers actively use Fog computing to support \gls{swot} deployments, which is why the introduced bias is not an issue for our survey.

\subsection{Identifying SWoT functions}
\label{subs:swot_functions}

When analyzing how the \gls{sw} technologies are integrated in the \gls{iot} in surveyed \gls{swot} publications, \textbf{different recurring functions have been identified, agnostic to the underlying technology stack and to the application domain}.
The concept of function in this work is understood as an elementary processing unit that can be combined with others in order to achieve a feature of higher order.

Functions have been extracted from existing practices in a bottom-up approach.
Identifying these functions depicted in the surveyed publications enabled the organization of contributions with respect to one another, easing their comparison. 
This organization supports the capture of a structured landscape of the contributions of the \gls{sw} to the \gls{iot}.
Our objective is
\begin{enumerate*}[label=(\roman*)]
	\item to characterize these \gls{swot}-enabling functions,
	\item to describe how they are instantiated in research contributions, and 
	\item to analyze the impact of Fog computing on their implementations.
\end{enumerate*}
\gls{swot} functions capture both the usage of \gls{sw} technologies for managing nodes themselves or their context, and the applicative content they are interested in. 
We identified three categories of \gls{swot} functions:

\begin{itemize}
	\item \textbf{Content value creation functions} are related to domain-specific activities, and focus on the \textbf{transformation} and \textbf{processing} of content relevant to an application. 
	Content is used as a neutral term to refer to any element in the classification provided by \cite{Rowley2007}, organized in the \gls{dikw} hierarchy.
	Tab. \ref{tab:content-oriented} lists papers contributing to these functions and Section \textsection \ref{subs:content-process} describes said functions in detail.
	\item \textbf{Node interworking functions} are not specific to an application domain but common to the \gls{iot} domain. 
	The development of such functions goes against vertical fracturation by focusing on the description of node characteristics, capabilities, and preferences.
	Node interworking functions allow nodes to be \textbf{aware} of their neighbors on the graph, and to offer a \textbf{homogeneous} self-representation. 
	Tab. \ref{tab:node-oriented} references the papers contributing to these functions, and they are described in Section \textsection \ref{subs:node-process}.
	\item \textbf{Node-Content dependency functions}, where the focus is not on content transformation but rather on facilitating access to content by leveraging node characteristics.
	Such functions, listed in Tab. \ref{tab:node-content}, are described in Section \textsection \ref{subs:node-content-processes}.
\end{itemize}

\subsection{Related work}
\label{subs:related_work}
Previous work has been done to survey the convergence between the \gls{iot} and the \gls{sw}: 
\begin{itemize}
	\item Early work in the \gls{swot} focused on semantic sensor networks. For instance, \cite{Compton2009} surveys sensor ontologies and observation representations. 
	The scope of this work is especially on sensor ontologies, and even if it proposes an overview of technologies enabling semantic sensor networks, it does not present specific applications relying on these ontologies. 
	Similarly, \cite{Szilagyi2016} gives an overview of the \gls{sw} stack applied to the \gls{iot}, and surveys \gls{iot} ontologies. 
	It goes beyond semantic sensor networks, but is still limited to model analysis. 
	We propose to focus on how the ontologies and the technologies of the \gls{sw} are used to develop the \gls{swot}, rather than on identifying exhaustively the models used.
	\item \cite{Atzori2010} is a survey of the \gls{iot} domain, proposing a definition for the notion of \gls{iot} and listing application domains and enabling technologies for the \gls{iot}. 
	The \gls{iot} paradigm is described as the convergence of Internet technologies, electronic devices, and \gls{sw} technologies. 
	However, the paper itself does not cover how \gls{sw} technologies are integrated into the \gls{wot}, while we intend to analyze in detail and compare different contributions to the \gls{swot}.
	\item \cite{PayamBarnaghi117} studies the roles \gls{sw} technologies can play in the \gls{iot}, as well as the challenges they represent. 
	This paper identifies some functions similar to what is presented in the present thesis in Section \textsection \ref{sec:contributions}. 
	%However, we situated the contributions to these processes within \gls{lmu} in order to enable a deeper understanding of how an \gls{iot} network is integrated into the \gls{swot}, and to give a finer-grained analysis grid: we want to identify the impact of the nodes on the processes they are involved into. 
	However, the deployments of these functions in a reference architecture is not studied, and the impact of technological constraints of the \gls{iot} on the \gls{swot} are no described in depth, as they are not in the intended scope.
	\item \cite{Jara2014} gives an overview of the evolution from the \gls{iot} to the \gls{wot} and toward the \gls{swot}. 
	It is focused on the role of standards in interoperability, and the integration of \gls{sw} technologies in standards. 
	It also provides an overview of technologies involved in the \gls{iot}. 
	This paper is oriented toward projects and industrial consortiums, which is complementary to our study. 
	We focus on the contributions of the \gls{swot} to \gls{iot} issues,and only integrate standardization concerns when they are related to this domain. 
	\item \cite{Perera2017} is a survey dedicated to Fog computing applied to the development of the smart city. 
	Knowledge management capabilities of Fog nodes are analyzed, and some \gls{sw} features such as semantic annotations are introduced, but the focus is primarily on the enablement of the smart city by Fog computing, rather than on the relationship between Fog nodes and \gls{swot} architectures.
	\item \cite{Sezer2018} gives a detailed overview of context-awareness in the \gls{iot}, and on the methodologies used to achieve it. 
	The authors focus more on techniques than on deployments: the role of Fog nodes is not considered. The survey gives a detailed analysis of the relationship between the \gls{iot} and the \gls{sw} domains, but also with Big Data and machine learning, which is out of the scope of the present survey. 
\end{itemize}

Overall, these surveys focus on the \gls{swot}, and on the implication of the integration of \gls{sw} principles and technologies in the \gls{iot}. 
To the best of our knowledge, the distinction between semantically-enabled and semantically-agnostic Fog nodes, and the respective roles of semantic Fog computing compared to semantic Cloud computing in supporting \gls{swot} deployments has not been the topic of any survey.

\input{035_survey/contributions.tex}
\input{035_survey/adaptations.tex}

\section{Conclusion}

In this chapter, a survey of the role of Fog nodes in supporting semantic processing in the deployment of the \gls{swot} has been proposed.
The Fog tier has previously been defined as an intrinsic component of the \gls{swot} architecture in Section \textsection \ref{subs:cloud_fog_device} with the Cloud-Fog-Device pattern.
With Fog nodes providing a necessary interoperability layer connecting \gls{iot} devices to Cloud nodes, we surveyed in this chapter the type of interoperability supported in the Fog tier.
Semantic interoperability may be enforced by semantic Cloud computing, in which case Fog nodes only provide technical and syntactical interoperability.
However, part of the \gls{sw} technical stack may be based on semantic Fog computing, in which case Fog nodes become semantic interoperability providers.
In order to be able to assess the role of Fog nodes in the \gls{swot}, we surveyed practices of the \gls{swot} domain situated in the reference Cloud-Fog-Devices architectural pattern.
Elementary functions have been identified through the projection of \gls{swot} contributions on this architecture, in an extension of work intially published in \cite{Seydoux2017}.
Contributions of the \gls{sw} to the \gls{iot} domain have been classified with respect to these functions, with a particular focus on the support role played by the semantic Cloud and Fog computing.
The complementarity of the Cloud and Fog paradigms has been confirmed by the trends observed among the surveyed papers.
\begin{itemize}
	\item Cloud nodes provide considerable computing resources, as well as a global context due to their overview of \gls{iot} deployments.
	\item Fog nodes provide limited computing resources, that are disseminated in the network and therefore closer to devices producing data.
	Since the Fog infrastructure is pervasive to the deployment, each Fog node only provides a local context for decision-making.
\end{itemize}

To complement this study, insights into the future of the \gls{swot} has been presented, exposing future functions and evolutions of the \gls{sw} developed to match constraints of the \gls{iot}.
This twofold approach to a \gls{swot} technical landscape showed the reciprocity of the convergence of the \gls{sw} and the \gls{iot} domains: 
\begin{itemize}
	\item not only does the \gls{sw} provide solutions to the interoperability and complexity issues of the \gls{iot}, but
	\item \gls{iot} constraints also challenge the \gls{sw} principles and technologies to evolve.
\end{itemize} 
The emergence of semantic Fog computing actively supports this convergence, providing promising solutions to the research challenges motivating the survey presented in this chapter.

The Fog paradigm promotes distributed approaches, by providing decentralized computing resources.
We discussed the role of \gls{sw} technologies and principles in support of interoperability in \gls{iot} architectures in the previous chapter \textsection \ref{chap:interoperability}.
In order to overcome \gls{iot} constraints, we also identified the complementarity of the Cloud and Fog paradigms.
Therefore, in the next chapter \textsection \ref{chap:decentralization}, the second set of contributions of this thesis is introduced, centered on the \textbf{Cloud-Fog collaboration} for semantic processing. 
Fog computing capabilities for scalability and \gls{qos} are leveraged with Cloud node capabilities for computation power and stability, as opposed to the volatility of a Fog infrastructure.
These contributions intend to take full advantage of semantic Fog computing in order to support the deployment of more scalable \gls{swot} architectures, adaptive to the dynamism of underlying \gls{iot} networks.
\chapter{Introduction}
\minitoc

\section{Interoperability and scalability issues in the IoT}

The ``Things'' of the \gls{iot} are now part of everyday life for many. 
To support this claim, one might consider the increasing number of smart cities including but not limited to Dublin (IE)\footnote{\url{http://smartdublin.ie/}}, Santander (ES)\footnote{\url{http://www.smartsantander.eu/}}, Milton Keynes (UK), San Francisco (US), New York (US), Jaipur (IN)\footnote{\url{http://smartcities.gov.in/content/}} or Yokohama (JA). 
All these municipalities are deploying \gls{iot} technologies to monitor city facilities such as street lightning, public transportation as well as environmental factors such as air quality, in order to offer innovative services to citizens. 

The growing integration of connected Things to human activities has an impact on a wide scope of application domains, such as environmental metering with sensor networks, transportation, home automation, e-health, agriculture, manufacturing, or smart user space (\textit{e.g.,} shopping malls, airports, parkings).
In the context of the \gls{iot} domain, the word ``smart'' is used to refer to devices whose core functionalities have been extended based on connection and computing capabilities: smartphones, smart watches, or smart fridges for instance. 
By extension, domains where such connected devices are integrated are also referred to as ``smart'', \textit{e.g.,} smart cities, smart homes or smart agriculture. 
Systems qualified as ``smart'' are thus meant to communicate with other ``smart'' devices in order to provide innovative services to their users depending on his/her requirements.
When user requirements are driving the conception of a system, principles of \textbf{user-centered design} apply \cite{Abras2004}.

One of the core principles of user-centered design is to \textbf{``empower the user to control the task''}.
In the case of \gls{iot}, such control starts with the installation of devices chosen directly by the user to match his/her needs.
However, the customizability of \gls{iot}-based applications is restricted by the approach of the industry so far, and has been oriented toward \textbf{vertical silos} \cite{Desai2015}. 
Proprietary systems are designed with a specific purpose, and on top of the devices the vendor also distributes the application serving the purpose. 
Siloed development may be an business model, chosen in order to build a closed ecosystem to keep customers captive, but it is also driven by technological constraints.
Developing a closed system is easier, and enables the implementation of optimizations dedicated specifically to the use cases intended by the manufacturer.
This approach cannot match the diversity of possible scenarios driven by user requirements, and raises \textbf{interoperability issues}.
Interoperability is the ability of systems to work together, and can be achieved in roughly two ways: 
\begin{itemize}
	\item either the interworked systems are natively compatible (\textit{e.g.,} by vendor-specific design, or through standardization), or
	\item a third-party acts as an interoperability provider.
\end{itemize}
Designing devices as black boxes hinders the development of third-party interoperability solutions, which is why vertical silos promote closed ecosystems: they favor vendor-specific interoperability.
Such restrictive vision limits the diversity of the potential applications, since deployments are locked by vendor design.
Users should be allowed to combine their connected devices in a \textbf{personalized fashion}, and application developers should be able to deploy generic applications that adapt to the available devices, which is totally opposed to vertical integration. 
Customizing one's \gls{iot} system requires devices to understand each other: \textbf{Things must be interoperable}. 
The extreme diversity of technologies involved in \gls{iot} deployments \cite{eclipse2018}, as well as the multiplicity of \gls{iot} standards, including but not limited to oneM2M\footnote{\url{http://www.onem2m.org/}}, OIC\footnote{\url{https://openconnectivity.org/}}, and LWM2M\footnote{\url{http://www.openmobilealliance.org}}, makes \gls{iot} interoperability a non-trivial issue. 
Moreover, standards do not solve interoperability issues entirely, and do not support the development of smart applications as depicted in the original vision of \cite{Berners-Lee2001}, where intelligent agents seamlessly interact with devices and information.
Beyond enabling the communication between entities, \textbf{understanding must be achieved}.

The heterogeneity of the \gls{iot} systems brings another issue: management complexity.
As stated in \cite{Zanella2014}, \cite{PayamBarnaghi117} or \cite{Foteinos2013}, the heterogeneity of an \gls{iot} system makes it hard to manage, especially on a large scale.
The more interactions there are, and the more technologies involved, the harder and the costlier human management can become.
Heterogeneous devices require different actions to obtain a similar result, which opposes another principle of user-centered design: ``follow natural mappings between intentions and the required actions'' \cite{Abras2004}.
Interoperability should therefore be used to give systems the ability to self-manage and self-configure, based on a shared understanding of \textbf{human-understandable high-level goals}. 

Disseminating myriads of devices in the environment also raises obvious \textbf{scalability issues}.
It is estimated that by 2020, \textbf{30 billion devices}\footnote{\url{https://internethealthreport.org/2018/spotlight-securing-the-internet-of-things/}} \cite{mozilla2018} will be connected by \gls{m2m} technologies, compared to the 0.9 bil\-lion connected in 2009 \cite{gartner2013}. 
The exponential growth of the number of connected devices is correlated with a dramatic increase in the volume of data they collect and exchange.
Therefore, \textbf{future-proof \gls{iot} devices and data management solutions must be designed for scalability} as well as interoperability. 

Early studies for \gls{iot} data management, as in \cite{5599715}, are mainly based on centralized approaches: data is collected and processed by a unique entity, usually a powerful remote Cloud-hosted server.
Such a design choice is motivated by the intrinsic constraints of \gls{iot} devices: the swarm of sensors and actuators on which the \gls{iot} system is based has a limited processing power, being more dedicated to the interaction with the physical world (i.e. sensing or acting).
Therefore, resource-consuming decision-making mechanisms based on large-scale data analysis cannot be deployed on peripheral devices, and they are centralized on powerful machines.
Cloud-based system design supports a smoother user experience by providing an interface reachable from any device and by hiding the complexity of the underlying infrastructure, while providing developers with on-demand provisioning and maintainability.
However, centralized architectures have flaws when used for to massively distributed systems such as for \gls{iot} systems deployment.
The drawbacks include response time, and the introduction of a single point of failure.
In such a centralized architecture, the Cloud server may become a bottleneck for communication and a threat for privacy.
Storing and processing a large data volume in a central place induces delay \cite{Shi2016} and may degrade the quality of service for \gls{iot} applications.
This may hamper the development of a variety of applications and inhibit the implementation of time-critical \gls{iot} applications.
Therefore, relying on a solely Cloud-based infrastructure is not satisfactory. 
The characteristics of Cloud architectures must be combined with a complementary paradigm appropriate for distributed systems.
In Cloud computing, scalability is envisioned in terms of horizontal expansion (more machines) or vertical expansion (increase of machine capabilities), but structurally data still needs to be concentrated in a central place before being processed.
An alternative approach is proposed by Fog computing, by taking advantage of the reduced processing power of equipments available close to \gls{iot} devices, in order to decentralize processing and to bring small pieces of computation executed locally.
%\Khalil{Style à revoir}
Two aspects of scalability are therefore considered, with different implications for the designed solutions. 
In order to enable emergent decentralized behaviors, an understanding of both its own components and the policies it should enforce, are necessary to the system.
Moreover, the interoperability solutions should be aware of the resource-constrained environment in which they are deployed.

\section{Enabling user centricity through the SWoT}

The will to produce machine-understandable knowledge has been associated to the idea of devices able to communicate directly with each other in order to extend their functionalities for a long time.
In his article proposing a reference definition for ontologies in computer science, \cite{Gruber1991} uses the formal description of electromechanical devices as an example of an ontology developed in a pilot project.
Even if the connected nature of these devices is not discussed, and was probably not part of the work produced at that time, the necessity to produce rich, meaningful device descriptions is still a challenge today.

In the scenario depicted by \cite{Berners-Lee2001}, the foundational article for the \gls{sw}, devices discover each other based on their descriptions.
In this vision, the extended capabilities of software agents and physical devices help to seamlessly provide complex services to human users. 
Important aspects of the scenario revolve around a user-centric approach: 
\begin{itemize}
	\item interoperability among devices and services
	\item the dynamic discovery process, adaptative to spatio-temporal constraints as well as to user preferences.
\end{itemize}
All of these concerns are at the core of the \gls{iot} domain in general, as coined in \cite{Kevin2009}.

As \cite{Corcho2010} and \cite{Murdock2016} point out, \textbf{\gls{sw} principles and technologies can provide solutions to the interoperability issues the \gls{iot} domain is facing}. 
The use of dereferencable vocabularies such as ontologies enables the capture of metadata in a machine-understandable way supporting a richer \gls{m2m} communication. 
Many recent research contributions from the \gls{sw} community introduce \gls{sw} capabilities (rich content description, reasoning...) into the \gls{wot} in order to develop the \acrlong{swot}, an evolution of the \gls{wot} where the \gls{iot} domain is extended with \gls{sw} principles and technologies. 

There is a natural dependency between the \gls{iot} and the \gls{sw} domains: since \gls{iot} devices communicate directly among themselves, in an \gls{m2m} fashion, no human agent can intervene in order to provide context or translation between two devices. 
Therefore, it is necessary, in order to enable the deployment of ubiquitous autonomic smart systems, to support the mutual understanding of devices.
Achieving global interoperability tends to enable the communication from anything to anything, not limited to physical devices.
This vision is referred to by Cisco as the \gls{ioe} \cite{Evans2012}, where \textbf{people, data and devices are interconnected}.
By supporting the representation of\textbf{ complex knowledge and reasoning processes}, the \gls{sw} also paves the way towards self-managing systems where reconfiguration actions are based on logical inference.

However, \gls{sw} technologies are resource-consuming, while \gls{iot} systems are characterized by resource-constrained devices.
Therefore, \textbf{the challenge in the development of the \gls{swot} is to support interoperability while adapting to the constraints of \gls{iot} systems}.

\begin{figure}
	\centering
	\caption{Overview of problematics and contributions}
	\label{fig:overview_contributions}
	\input{010_intro/contributions_overview.tex}
\end{figure}

The work presented in this thesis proposes contributions in the context of the \gls{swot}, and aims at elaborating approaches to tackle this issue.
An overview of the contributions is provided in Fig. \ref{fig:overview_contributions}.
For the sake of clarity, and in order to support the technical description of contributions proposed in this dissertation, Chapter \textsection \ref{chap:background} provides background information about the \gls{iot} and the \gls{sw} domains. 
For both, a contextual and technical overview is provided, leading to introducing the emergence of the \gls{swot} from the convergence of the \gls{iot} and the \gls{sw} principles and technologies.
An architectural pattern supporting \gls{swot} deployments is also described.

In Chapter \textsection \ref{chap:interoperability}, the first contributions of this thesis are presented, focusing on interoperability in the \gls{swot}.
Interoperability being a core concern of the \gls{sw}, and the drive for the emergence of the \gls{swot}, it is necessary to define exactly the aspects of interoperability that are in the scope of the present work.
The role of \gls{swot} standards is briefly discussed, before considering how ontologies can support interoperability in the \gls{swot}.
The multiplicity of ontologies dedicated to the \gls{iot} led us to propose a set of requirements to measure the quality of \gls{iot} ontologies.
We instantiated these requirements with IoT-O, a core-domain modular IoT ontology, referred to as \textbf{contribution I.A}. 
The role of IoT-O in the support of interoperability is discussed in three use cases centered around a smart building:
\begin{itemize}
	\item an open data,
	\item a federated data hub,
	\item a home automation use case.
\end{itemize}
Self-management of \gls{swot} systems is addressed in the latter, by proposing \textbf{technical contribution I.B}, namely semIoTics, an autonomic computing system.

Since the considered problematic is twofold, first focusing on interoperability and then on \gls{iot} systems scalability and computational power constraints, the description of related work is distributed in the appropriate chapters.
However, a complete chapter is dedicated to surveying the inter-dependence of interoperability and \gls{iot} system constraints in the \gls{swot}.
In Chapter \textsection \ref{chap:survey}, the decentralization of the \gls{sw} technological stack is studied in a state-of-the-art of \gls{swot} deployments.
Recurring functions of \gls{sw} technologies in \gls{iot} architectures are identified, and they are used to characterize the role of Fog-enabled architectures for supporting the development of a decentralized \gls{swot}.

The survey proposed in Chapter \textsection \ref{chap:survey} builds the context for the second contribution of this thesis, presented in Chapter \textsection \ref{chap:decentralization}.
In order to adapt the \gls{swot} solutions to \gls{iot} systems constraints, both in terms of processing power and scalability, we propose a generic approach to dynamically distributed rule-based reasoning, where Cloud and Fog nodes collaborate to achieve an emergent distribution of processing, called \gls{edr}, and referred to as \textbf{contribution II.A}. 
Being a generic approach, \gls{edr} is agnostic to application-level requirements. 
Therefore, \textbf{technical contribution II.B} is a refinement of \gls{edr}, called \edrt, that implements a deployment strategy aiming at bringing rule computation as close as possible to sensors producing data.
Such strategy is devised to fulfill a delay reduction requirement, from the collect of observations to the delivery of deduction to the application.
The performances of \edrt are studied in two use cases detailed in Chapter \textsection \ref{chap:experimentations}, dedicated to home automation and smart manufacturing.
Finally, limits of the current contributions and perspectives for future work are considered in Chapter \textsection \ref{chap:conclusion}, the concluding chapter of the present thesis.

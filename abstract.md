IoT systems are deployed in human environments seeking to extend the functionalities of everyday appliances by providing connectivity and computing power.
One of the purposes of deploying "smart" devices being to offer innovative services to users, principles of user-centric design should apply.
In particular, users should be able to install IoT devices in their environment according to their own needs.
However, the technical heterogeneity of IoT devices combined with the multiplicity of IoT vertical use cases creates interoperability barriers preventing such customizability.
Beyond technical interoperability issues, semantic interoperability is a core enabler of user-centered IoT systems.
In order to provide such semantic interoperability, the Semantic Web principles and technologies have been used in the literature to support IoT architectures, leading to the emergence of the SWoT domain.
However, a divergence exists between IoT systems and Semantic Web technologies, hindering the development of SWoT systems.
The IoT is characterized by constrained devices, with restricted computing and communication resources, while Semantic Web technologies are resource consuming.
The constant growth of IoT deployments also raises crucial scalability issues.

The work presented in the present thesis is twofold: both interoperability and resource-constraints issues shaping the emergence of the SWoT domain are considered.
The first discussed contributions address the enablement of interoperability in IoT systems by adopting to Semantic Web principles and technologies.
After considering the role of standards in the SWoT domain, major IoT ontologies are studied.
Design principles for IoT ontologies are identified, and applied for elaborating IoT-O, a core-domain modular IoT ontology.
IoT-O is then used as an interoperability enabler in multiple IoT knowledge management use cases.
In particular, IoT-O drives SemIoTics, an autonomic computing software we designed for home automation.
The second set of contributions aim at tackling resource constraints inherent to the IoT while proposing a scalable solution.
For this purpose, a generic approach for dynamically distributed rule-based reasoning is proposed, called EDR.
EDR aims at defining an technique for rule distribution driven by application-level requirements.
It is then refined by EDRt, that implements an application-level strategy dedicated to bringing rule execution as close as possible to IoT data producers in order to reduce the delivery delay of deductions.
EDRt is evaluated in order to measure the performances and the scalability of the proposed approach.
We contextualize these two contributions with a survey studying the role of semantic Fog computing as a support for SWoT architectures.
This survey focuses of recurrent pattern identified in SWoT systems, and studies how these patterns are instantiated within a SWoT architectural pattern we characterized.

------------------------------------
Les systèmes IoT sont déployés dans l'environnement de leurs utilisateurs dans le but d'étendre les fonctionnalités d'objets de tous les jours en leur attribuant des capacités de communication et de calcul.
L'un des objectifs de ces déploiements d'objets dits "intelligents" étant d'offrir des services innovants aux utilisateurs, les principes de conception centrés utilisateurs doivent être pris en compte.
En particulier, les utilisateurs devraient avoir la possibilité d'installer dans leur système les objets connectés correspondant le plus à leurs besoins.
Cependant, l'hétérogénéité technique des objets connectés, associée à la multiplicité des domaines verticaux dans lequel le domaine de l'IoT a un impact, crée des problématiques d'interoperabilité qui limitent le degré de personnalisation possible.
Au-delà de l'interoperabilité technique, c'est l'interoperabilité sémantique qui est un élément central d'un système IoT centré sur ses utilisateurs.
Pour obtenir une telle interopérabilité, les principes et les technologies du Web Sémantique ont été utilisés dans la littérature pour soutenir les architectures IoT, menant à l'émergence d'un nouveau domaine, le Semantic Web of Things (SWoT).
Une divergence existe cependant entre les systèmes IoT et les technologies du Web Sémantique, restraingnant le développement du domaine du SWoT.
En effet, le domaine de l'IoT est caractérisé par la présence d'objets contraints, dont les capacités de communication et de traitement sont limitées, alors que les technologies du Web Sémantique sont consommatrices de ces ressources.
La croissance constante du nombre d'objets connectés pose aussi de sérieux problèmes de passage à l'échelle.

Les travaux présentés dans cette thèse ont donc une double portée : il s'agit à la fois de promouvoir l'interoperabilité, tout en proposant des solutions pour le SWoT qui soient adaptées aux ressources contraintes du milieu dans lequel on les déploie.
La première contribution présentée est particulièrement centrée sur l'interopérabilité qu'amènenent des principes et les technologies du Web Sémantique. Après une présentation de standards de l'IoT en lien avec la problématique de l'interoperabilité sémantique, les principales ontologies de l'IoT sont décrites.
Des bonnes pratiques pour la conception d'ontologies pour l'IoT sont caractérisées, et appliquées dans le proposition de IoT-O, une ontologie modulaire coeur de domaine pour l'IoT.
L'utilisation d'IoT-O comme vecteur d'interoperabilité est ensuite illustrée dans plusieurs scénarios de gestions de connaissance pour l'IoT.
En particulier, IoT-O est centrale dans le fonctionnement de semIoTics, un logiciel de gestion autonomique déployé pour l'automatisation d'un appartement.
Le second volet de contributions présentées dans cette thèse vise à s'attaquer au problème des contraintes en ressources inhérentes au domaine de l'IoT en proposant une solution pouvant passer à l'échelle.
À cet effet, une approche générique de distribution dynamique de raisonnement à base de règle est proposée, appelée EDR.
EDR a pour but la distribution de règles, en prenant en compte les besoins applicatifs.
Cette approche générique est ensuite raffinée par EDRt en implémentant une stratégie cherchant à amener les règles au plus près des producteurs de données (les capteurs), pour réduire le délai d'envoi des déductions produites par les règles aux applications.
L'évaluation d'EDRt vise à mesurer ses performances, et sa capacité de passage à l'échelle.
Ces deux contributions sont contextualisées par un état de l'art caractérisant le rôle du Fog computing dans le domaine du SWoT.
Des motifs récurrents ont été identifiés dans l'utilisation des technologies et des principes du Web Sémantique dans l'IoT, et cet état de l'art replace ces pratiques dans un patron architectural que nous avons proposé.

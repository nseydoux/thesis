\chapter*{Résumé en français}

\section*{Introduction}

Les objets connectés représentent une nouvelle étape dans la miniaturisation et la diffusion dans l'environnement des dispositifs informatiques. 
Des capteurs et des actionneurs sont disséminés afin de permettre l'élaboration de nouveaux scénarios pour une agriculture de précision, un habitat automatisé, ou encore une ville dite intelligente.
La diversité des cas d'utilisation entraîne une diversité de besoins, laquelle induit une hétérogénéité tant materielle que logicielle parmi les objets connectés.
Cette hétérogénéité est la source de l'un des verrous qui pèse sur le développement des objets connectés : le manque d'interopérabilité. 
Des objets sont dits interopérables s'ils peuvent échanger des services ou des informations. 

Selon le point de vue de la conception centrée sur l'utilisateur \cite{Abras2004}, la capacité de l'utilisateur à contrôler l'exécution d'une tâche effectuée par un système est primordiale.
Une première étape dans l'ouverture de ce contrôle réside dans la liberté de choisir les éléments qui composent le système déployé, plutôt que d'être contraint par un écosystème limité.
C'est là que les limitations imposées par le manque d'interopérabilité commencent. 

Afin de garantir l'interopérabilité, des standards ont été développé dans le domaine de l'internet des objets.
De plus, afin d'offrir une interopérabilité la plus profonde possible, ces standards ont été complétés par l'utilisation des techniques et des principes du Web Sémantique (WS). 
L'interaction entre le domaine de l'IoT et le domaine du WS a amené l'émergence d'un nouveau domaine, le Semantic Web of Things (SWoT).
Cependant, déployer les technologies du Web Sémantique dans des réseaux dans lesquels sont présents des objets très contraints n'est pas trivial.
En effet, les représentations de connaissances et les principes qui sous-tendent ces technologies tendent à être consommateurs de ressources, ce qui est incompatible avec la sobriété énergétique et calculatoire imposée par la nature des objets connectés.

La problématique de cette thèse est donc double : il s'agira de discuter les intérêts en terme d'interopérabilité du développement du SWoT, tout en proposant des solutions permettant d'adapter les technologies du Web Sémantique aux contraintes de l'IoT.
La suite de ce résumé présente le contenu des chapitres de cette thèse dans laquelle mes principales contributions sont décrites.
Tout d'abord, le Chapitre \textsection \ref{chap:interoperability} détaille le rôle du Web Sémantique en tant que support de l'interopérabilité, avec en particulier la proposition de IoT-O, une ontologie pour l'IoT désignée comme contribution I.A. 
L'utilisation de IoT-O est illustrée dans la contribution I.B à travers semIoTics, un système autonome de contrôle d'un habitat connecté.
Le Chapitre \textsection \ref{chap:survey} propose un état de l'art analysant la place du Fog computing (une technique de calcul distribué) dans le déploiement du SWoT. Cette analyse s'appuie sur un patron architectural de référence pour les réseaux d'objets implémentant les principes du SWoT: le Cloud-Fog-Device.
Dans le Chapitre \textsection \ref{chap:decentralization}, la nature répartie des réseaux d'objets connectés est mise à contribution pour décentraliser le raisonnement, à travers une approche générique de distribution dynamique de raisonnement à base de règles, appelée EDR. 
EDR est la contribution II.A de cette thèse.
Elle est complétée par \edrt, une version raffinée de EDR implémentant une stratégie particulière de gestion des règles.
Les performances de \edrt sont détaillées dans le Chapitre \textsection \ref{chap:evaluation}.

\section*{Le SWoT, porteur d'interopérabilité}

Que ce soit par choix d'un modèle économique, ou contraint par des considérations techniques, beaucoup de systèmes d'objets connectés ont été développées par leurs concepteurs dans un modèle d'intégration verticale, dit "en silo".
Une telle approche implique que la couche applicative, le protocole de communication et la couche matérielle sont des systèmes fermés, avec lesquels il n'est pas possible d'interagir facilement depuis un système tiers.

Pour pallier ce manque d'interopérabilité, il est courant d'avoir recours aux standards, tels que oneM2M ou le Web of Things du W3C.
Ces standards ont ceci de particulier qu'ils considèrent non seulement l'interopérabilité syntactique, en harmonisant les schémas représentant les informations, mais aussi l'intéroperabilité sémantique, en promouvant des vocabulaires et techniques du Web Sémantique. 

En particulier, le Web Sémantique supporte l'interopérabilité en proposant des vocabulaires formels décrivant un domaine de connaissance donné, que l'on appelle ontologies \cite{Gruber1991}.
Il existe de nombreuses ontologies pour caractériser le domaine de l'IoT, et la première partie de notre travail a consisté à lister ces ontologies pour identifier celles qui satisfont un ensemble de besoins que nous identifions comme des critères de qualité, inspirés par la méthode NeOn \cite{MariadelCarmenSuarezdeFigueroaBaonza99}.
Nous distinguons deux types de critères : 
\begin{itemize}
	\item Des critères \textbf{conceptuels}, c'est à dire les domaines couverts par l'ontologie. 
	L'IoT étant un domaine étalé, plusieurs sous-domaines y sont rattachés : Capteurs et Observations, Actionneurs et Actions, Objets et Agents virtuels, Services, Gestion de l'énergie, et enfin Cycle de vie.
	\item  Des critères \textbf{fonctionnels}, déterminant des caractéristiques techniques de l'ontologie.
	Ces critères cristallisent un certain nombre de bonnes pratiques visant à faciliter la réutilisation des ontologies d'un projet à l'autre, les rendant ainsi interopérables.
	L'accessibilité en ligne, la modularité \cite{Aquin2012} ou l'appui sur des patrons de conception \cite{Gangemi2005} font partie des critères identifiés.
\end{itemize} 

Face à l'absence d'ontologies qui couvre l'ensemble des critères identifiés, nous proposons \textbf{IoT-O}, \textbf{une ontologie modulaire coeur de domaine pour l'IoT}. La description de IoT-O a été publiée dans \cite{Seydoux2016}.
Cependant, pour ne pas tomber dans un travers que nous avons observé dans le domaine, IoT-O réutilise des ontologies déjà existantes, de manière à ne pas redéfinir de concepts.
Chacun des domaines identifiés dans les critères conceptuels est couvert par un module de IoT-O, et l'ontologie IoT-O en tant que telle est le module de plus haut niveau qui établit des liens entre les autres.
Les modules de IoT-O, ainsi que les ontologies qui les composent, sont représentés dans la Figure \ref{fr:fig:ioto_modular}.

\begin{figure}
	\centering
	\caption{Dépendances des modules de IoT-O}
	\label{fr:fig:ioto_modular}
	\scalebox{0.85}{
		\input{030_interoperability/figures/tikz/ioto_modules.tex}
	}
\end{figure}

IoT-O est par exemple utilisée par semIoTics, un logiciel de gestion autonomique déployé pour automatiser le contrôle des objets dans un appartement connecté. 
semIoTics implémente la boucle MAPE-K \cite{Kephart2003}, et s'appuie sur une base de connaissance dans laquelle l'appartement et ses caractéristiques, mais aussi les objets qu'il contient et les services que ces derniers implémentent sont décrits avec les différents modules de IoT-O.
La mise en place de semIoTics est aussi rendue possible par l'utilisation d'OM2M\footnote{\url{http://www.eclipse.org/om2m/}}, un logiciel libre implémentant le standard oneM2M.
OM2M permet d'assurer l'interopérabilité syntactique vers les différents objets déployés dans l'appartement. 
Une question se pose cependant : dans le cas de semIoTics, les décisions sur les actions à effectuer sont prises sur un serveur disposant d'importantes capacités de calcul, et ces décisions sont converties en actions atomiques transmises directement aux actionneurs concernés. 
On observe là une séparation nette entre la partie du réseau dans laquelle les informations ont été sémantisées, et la partie dans laquelle les objets sont trop contraints pour manipuler ces représentations plus complexes.
Dans la suite de cette thèse, des approches distribuées sont considérées afin de rapprocher au maximum cette séparation entre données brutes et informations sémantisées des objets connectés.

\section*{Quel est le rôle du Fog computing dans le domaine du SWoT ?}

Pour analyser comment est répartie la charge de calcul imposée par l'utilisation des technologies et des formalismes du Web Sémantique dans le domaine du SWoT, identifier un patron architectural récurrent dans les déploiement SWoT est une première étape.
On trouve d'un côté de ce patron les serveurs puissants et accessibles de manière globale à travers le Web (aussi appelés serveurs du Coud), et de l'autre les objets connectés contraints, avec leurs protocoles de communication propres.
Entre les deux, la communication n'est pas forcément directe : des dispositifs sont souvent utilisés pour assurer l'interface entre les technologies Web côté serveur, et les technologies spécifiques côté objets.
Ces dispositifs sont en général appelés passerelles, et on en trouve dans de nombreuses architectures de la littérature telles que décrites dans \cite{Su2018}, \cite{Alaya2015}, \cite{Zanella2014} ou \cite{Liu2015}.
Or, si l'on se réfère à la définition données par l'Open Fog Consortium\footnote{\url{http://openfogconsortium.org/}}, le Fog est une architecture distribuant services et ressources entre le Cloud et les objets.
Par cette définition, le Fog est un composant intrinsèque du SWoT, d'où notre proposition d'un patron architectural, le Cloud-Fog-Device, qui capture les trois niveaux identifiés.

En partant de ce constat, il est intéressant d'étudier le rôle du Fog computing dans l'intégration des technologies du Web Sémantique dans les réseaux IoT.
C'est pourquoi le Chapitre \textsection \ref{chap:survey} est centré sur un état de l'art dans lequel des fonctions récurrentes remplies par les technologies du Web Sémantique dans le SWoT sont identifiées.
Le déploiement de ces technologies est situé dans le patron Cloud-Fog-Device, pour distinguer deux cas de figures : 
\begin{itemize}
	\item Soit les noeuds du Fog se voient délégué des fonctionnalités sémantiques, auquel cas on peut parler de ``Semantic Fog Computing'' : le Fog joue un rôle passif dans l'obtention de l'interopérabilité sémantique.
	\item Soit les noeuds Fog n'ont aucune fonctionnalité en lien avec les technologies du Web Sémantique, auquel cas on parle de ``Semantic Cloud Computing'' : le Fog a un rôle d'interopérabilité technique ou syntactique, mais il est passif pour l'interopérabilité sémantique.
\end{itemize}

Cet état de l'art permet de souligner l'intérêt d'une approche distribuée en mettant en avant les caractéristiques des déploiements appuyés sur un Fog actif dans le déploiement des technologies sémantiques pour le SWoT.
En particulier, le besoin de scalabilité est mis en avant, ainsi que l'augmentation de la responsivité rendue possible par le raisonnement plus local.
C'est pourquoi, dans le chapitre suivant, une approche décentralisée est proposée, pour proposer un SWoT qui s'adapte aux contraintes de l'IoT.

\section*{Adapter les principes du SWoT aux contraintes de l'IoT}

Dans le Chapitre \textsection \ref{chap:distribution}, nous proposons la seconde contribution théorique et technique de cette thèse. 
Afin de bénéficier des avantages des solutions de SWoT distribuées, et pour permettre de marier les capacités offertes par le Web Sémantique avec les contraintes de l'IoT, nous proposons EDR (contribution II.A), une approche  générique de distribution dynamique de raisonnement à base de règles.
EDR repose sur l'architecture hiérarchique capturée par le patron Cloud-Fog-Device. 
C'est une approche basée sur un vocabulaire dédié qui permet la propagation de règles SHACL modulaires en contrôlant le comportement des noeuds du réseau.
EDR est décorrélée de la stratégie qui guide la propagation de règles vers des noeuds en particuliers, c'est pourquoi EDR est une approche dite générique, qui peut être raffinée en implémentant une stratégie particulière.
Afin d'être compatibles avec l'approche proposée, les règles doivent se composer de plusieurs modules : 
\begin{itemize}
	\item Un module ``c\oe ur'', qui contient la partie métier de la règle.
	\item Un module d'activation, qui permet à un noeud de déterminer s'il peut appliquer la règle
	\item Un module de transmission, qui permet à un noeud de déterminer auquel de ses voisins il peut transmettre la règle
	\item Un module notification, qui permet à un noeud de déterminer à qui envoyer les résultats de la règle quand celle-ci est appliquée
\end{itemize}

Les modules d'acivation, de transmission et de notification sont tous liés à la stratégie de propagation des règles.
Les décisions qu'ils permettent s'appuient sur la base de connaissance de chaque noeud : les décisions sont prises localement.
Pour rendre ce mécanisme possible, les noeuds échangent des informations pour se représenter les capacités de leurs voisins et l'état de leur environnement.

Afin de démontrer la faisabilité d'EDR, et pour montrer l'intérêt de cette approche, nous proposons en tant que contribution II.B \edrt, un raffinement d'EDR implémentant une stratégie qui vise à propager les règles le plus bas possible dans le réseau.
\edrt est basée sur les types de données observées par les capteurs, et consommées par les règles qui sont propagées. 
Ainsi, on cherchera à ne pas propager une règle consommant un type de donnée $\delta$ dans un sous-arbre du réseau dans lequel un tel type de donnée n'est pas produit.


\section*{Conclusion}


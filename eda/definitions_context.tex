\newcommand{\surveyedPapers}{65 }
\newcommand{\fogActiveRate}{54}

\subsection{Is the Fog an intrinsic component of the SWoT ?}
\label{sec:fog_iot}

The purpose of this survey is to discuss the relationship between Fog computing and the \gls{swot}. In order to do so, this section describes the context of the survey we conducted. Definitions for the key terms are proposed in subsection \ref{subs:definitions}, the survey methodology is described in subsection \ref{subs:survey_methodo}, and the elements of the reading grid used for classifying contributions are presented in subsection \ref{subs:swot_architecture}. Related surveys are presented in subsection \ref{subs:related_work}.

\subsubsection{Background and definitions}
\label{subs:definitions}
To assess the role of the fog in the \gls{swot}, a clear definition of these terms must first be adopted, as well as a reading grid to perform such assessment. 

\paragraph{IoT vs SWoT, Cloud vs Fog}

In order to define what is the \gls{swot}, one must first define what is the \gls{iot}. 
The term "IoT" was coined in 1999 by \citeauthor{Ashton2009}\cite{Ashton2009}, and initially referred to networks of RFID tags. 
The meaning of the word evolved with the emergence of active computing power ubiquitously deployed in connected devices. 
A borader definition of the \gls{iot} is provided in \cite{Perera2014_context}, embracing the diversity of nature and purpose of the so-called Things: 
``The Internet of Things allows people and things to be connected anytime, anyplace, with anything and anyone, ideally using any path/network and any service''.
We adopt this definition in this paper: the term \gls{iot} refers to the the area of technology and research enabling the deployment of devices networks. 
These devices being often constrained, dedicated technologies are required to support their communications, hindering the ability of applications to interact with devices in a generic approach, leading to interoperability issues.
To bridge the gap between applications and the \gls{iot} on a technical level, web principles and technologies have been used. 
Around 2008, the notion of \gls{wot} was introduced in work such as \cite{Stirbu2008} or \cite{Guinard2009}. 
On top of the heterogeneous \gls{iot} communication network, the \gls{wot} provides a unified access to both data and devices identified with \gls{iri} through web protocols. 
The \gls{wot} is standardized by the W3C\Nicolas{à développer ou à retirer ?}. 
However, the interoperability brought by the web technologies is only technical, and does not allow entities to understand the messages they exchange. 
That is why in parallel to the \gls{wot}, the \gls{swot} was developed \cite{Scioscia2009, Pfisterer2011} in order to achieve semantic interoperability for the \gls{iot}. 
The notion of \textbf{\gls{swot}} covers the \textbf{integration of semantic web principles and technologies in the \gls{iot}}, which is often but not necessarily conjoined with the \gls{wot}. 
A sensor network where observations are annotated with an ontology, but where resources are not accessible through HTTP, would be an example of a \gls{swot} instantiation not based on the \gls{wot}.

The second important notion in the topic addressed by the present survey is the notion of \textbf{Fog} and Fog computing, a paradigm that was proposed in 2011 \cite{Bonomi2012} as an alternative to Cloud computing suitable for the \gls{iot}. 
The NIST gave a reference definition for Cloud computing \cite{Mell2011}, but the concept was developed in earlier work\addRef. 
Cloud resources are characterized by their network accessibility and their elasticity, making them an interesting relay for \gls{swot} applications. 
Data is collected at a large scale by \gls{iot} devices, and then concentrated on the Cloud where it is processed, stored, and accessed by applications. 
However, \cite{Bonomi2012} points out the limitations of this model for latency-sensitive applications, and propose to use processing power and storage capabilities located at the edge of network to complement Cloud resources. 
A distributed approach also provides more resilience by removing the single point of failure, and is more scalable \cite{Dastjerdi2016}.
Offloading computation from the Cloud to devices at the edge of the network is a very active research domain, with many interconnected concepts, such as \gls{mec}, Cloudlets, or Mist computing \cite{Patel2017}.
Fog computing is defined by the open fog consortium\footnote{\url{http://openfogconsortium.org/}} as a "system-level horizontal architecture that distributes resources and services [...] anywhere along the continuum from Cloud to Things"
\textbf{Fog nodes are massively distributed, heterogeneous, and they provide limited processing power between data sources and the Cloud}.
With this definition, standard \gls{iot} gateways, connecting Things to the Cloud, are part of the Fog.
Since the \gls{swot} has been initially deployed in the Cloud based on data collected by Things, there is a necessary layer to connect the ad-hoc \gls{iot} networks to the Cloud.
Therefore, \textbf{the Fog is intrinsically part of the \gls{swot}}.

\paragraph{Node and SWoT functions}

Being a network of Things connected together, the \gls{iot} can be seen as a graph where the Things are vertexes and their connections are the edges. 
So far, the notion of Thing has been used to indifferently refer to devices (sensors and actuators) and to services. 
This choice is justified by the similarity of the roles devices and services have on \gls{iot} networks: a service can be seen as the endpoint of a device for the other nodes of the \gls{iot}, and a device can be seen as the physical implementation of a service, depending on the chosen perspective.
That is why we introduce here the notion of \textbf{node, an abstraction covering both device and service} concepts. 
Fundamentally, \textbf{a node is an active entity that can be addressed on the network}. 
An active entity is able to send and/or receive requests, which is the case for both devices and services.  
This notion of node is already present in architectures described in \cite{Alaya2015} or in \cite{Perera2014_context}.

When analyzing how the Semantic Web technologies are integrated in the \gls{iot} in surveyed publications, \textbf{different recurring functions are identified, agnostic to the underlying technology stack and to the application domain}.
The concept of function in this work is understood as an elementary unit, that can be combined with others in order to achieve a functionality of higher order.
Functions are characterized from practices in a bottom-up approach.
Identifying these functions depicted in the surveyed publications enabled the capture of a structured landscape of the contributions of the semantic web to the \gls{iot}, by comparing these contributions, and to organize them with respect to one another.
The aim of this paper is to characterize these \gls{swot}-enabling functions, to describe how they are instantiated in research contributions, and what is the impact of the Fog on them. 
\gls{swot} functions capture both the usage of Semantic Web technologies for managing Nodes themselves, or their context and the applicative content they are interested in, and are divided in three categories.
\textbf{Content value creation functions} are related to domain-specific activities, and focus on the \textbf{transformation} and \textbf{processing} of content relevant to an application. 
Content is used a a neutral term in the classification provided by \cite{Rowley2007} as data, information or knowledge, organized in the \gls{dikw} hierarchy.
Tab. \ref{tab:content-oriented} lists papers contributing to these functions, and Section \ref{subs:content-process} describes said functions in details.
\textbf{Node interworking functions} are not specific to an application domain, but common to the \gls{iot} domain. 
The development of such functions goes against vertical fracturation, by focusing on the description of nodes characteristics, capabilities and preferences.
Node interworking functions allow nodes to be \textbf{aware} of their neighbours on the graph, and to offer a \textbf{homogeneous} self-representation. 
Tab. \ref{tab:node-oriented} references the papers contributing to these functions, and they are described in Section \ref{subs:node-process}.
Finally, in \textbf{Node-Content dependency functions}, the focus is not on content transformation, but rather on facilitating access to content by leveraging nodes characteristics.
Such functions, listed in Tab. \ref{tab:node-content}, are described in Section \ref{subs:node-content-processes}.

\subsubsection{Survey methodology}
\label{subs:survey_methodo}

This survey aims at identifying how the Fog enables the development of the \gls{swot}, and it has been conducted according to the systematic literature search technique presented in \cite{Kitchenham2004}.
More precisely, since it has been established that the Fog cannot be separated from the \gls{swot}, the question is whether \gls{swot} functions are only deployed in the Cloud, with the Fog being a ``passive'' interoperability layer, or \textbf{if the Fog plays an active role in the implementation of these \gls{swot} functions}. 
To answer this question, papers have been collected from major search engines (namely IEEEXplore, ScienceDirect and ACL Digital Library) with the keywords ``SWoT'', ``Semantic Web'', and ``Internet of Things''. 
Only journal and conference papers have been considered, workshop and poster papers were excluded. 
Moreover, in order to focus on papers relevant to the research question, only papers in which the deployment architecture is clearly described are considered.
Such a description can be done in a figure, by providing hardware capabilities of the deployment architecture, or by describing the capabilities of the deployment nodes.
Contributions based on a purely functional architectures are excluded from the survey if the components are not situated on identified containers (Cloud or Fog).
Eventually, \surveyedPapers publications have been considered for the survey, sorted by year of publication in Tab. \ref{tab:paper_per_year}. 

\begin{table}
	\centering
	\csvautotabular{csv/papers_per_year.csv}
	\label{tab:paper_per_year}
	\caption{Surveyed publications, per year}
\end{table}

This table shows an increase of the number of papers relevant to our survey with time, with a proportional increase of the papers in which the Fog plays an active role in \gls{swot} functions. 
Overall, semantic capabilities are distributed in the Fog in \fogActiveRate\% of the studied papers. 
It should be noted that this rate is biased by the inclusion criteria for papers selected in this study: by default, papers where the Fog is inactive from the \gls{swot} standpoint do not provide details about their deployment architecture. 
The choice to exclude such papers from the survey was made because their analysis would have been entirely based on an assumption that is directly correlated with the research question we want to discuss. 

\subsubsection{Cloud-Fog-Devices, a widespread three-tiered architectural pattern for the SWoT}
\label{subs:swot_architecture}

\textbf{Not all nodes of an \gls{iot} network are equivalent}: some devices are very constrained, whereas the servers providing analytic capabilities are powerful machines. 
Three homogeneous types of nodes are identified, matching with definitions provided in section \ref{subs:definitions}: Cloud, Fog, and constrained devices. 
Different characteristics are used to cluster nodes into meaningful classes, summarized in Tab. \ref{tab:node_characs}. 
The core characteristic of a node is its \textbf{processing power}, i.e. its ability to apply treatments of varying complexity to content. 
The processing power also determines the ability of the node to process content of a varying expressivity, from the very simple agreed-upon byte array to the much more complex \gls{kb} instantiations. 
The higher a node's processing power is, the more expressive content it can handle, and the more complex operations it can achieve. 
Nodes are also characterized by their \textbf{memory}, i.e. the quantity of information it can hold at a given time, and \textbf{storage} capability. 
Storage is the available space giving access to persistent content. 
The notion of \gls{iot} is inseparable from the notion of connectivity, and a node can also be classified according to its \textbf{communication capabilities}. 
These capabilities include the protocols it supports, its general availability on the network, and its bandwidth. 
Nodes also differ by the nature of their energy source: while some nodes are attached to traditional power grids, other nodes, deployed in the field, are reliant on batteries, or on renewable energy sources like solar panel, or energy harvesting. 

\input{tables/table_node_caracs}

The definitions for these three clusters are loose enough to be projected onto similar notions presented in surveyed papers, and the relevance of such approach is discussed therafter by showing how existing architectures relate to these three tiers.

\begin{itemize}
	\item \textbf{Cloud nodes} are the "weakly constrained nodes" of \cite{Zanella2014}, Infrastructure Node in \cite{Alaya2015}, and are directly characterized as Cloud in \cite{Liu2015, Szilagyi2016}. In this category, we classify Cloud or local servers as well as powerful devices with respect to the remaining of the deployment, potentially including standard laptops and domain-specific mobile robots or machines with powerful computation capabilities embedded. They have high processing power, extended communication capabilities, and large storage capabilities. For instance, in \cite{DahnLe-Phuoc18}, Cloud nodes are in charge of applying data fusion operators to data streams. 
	\item \textbf{Fog nodes} are very often referred to in the literature as \textbf{gateway} (\cite{Compton2009}, \cite{Alaya2015}, \cite{Desai2015}), because they are bridges between Cloud nodes and more constrained devices. They are usually dedicated to content transformation and protocol bridging: in \cite{PayamBarnaghi118}, fog nodes are presented as intelligent nodes where content can be converted from its raw representation to a richer one. \cite{Desai2015} proposes an architecture where \textbf{the gateway is a contact point between the \gls{iot} and the \gls{swot}}, performing both annotation and protocol proxying. In \cite{Nikoli2011} and \cite{Zanella2014}, fog are proxies for wireless devices networks. Fog nodes typically conform with the definition provided in section \ref{subs:definitions} : medium processing power, extended communication capabilities, and restricted memory storage. They are nodes where content gathered by sensors or sensing services is collected, transformed, and redistributed. 
	\item \textbf{Devices} typically have very limited power source, processing and communication capabilities, and little to no storage capabilities. These are by definition present in every \gls{iot} architecture, and in direct contact with the physical world. In some papers such as \cite{DahnLe-Phuoc18} or \cite{Vlacheas2013}, these nodes are not directly present, their representation is wrapped by a \gls{mn}. The \gls{ln} were in early studies mainly sensors, like in \cite{Compton2009}, and evolved toward diverse nodes including actuators, displays and composite devices in more recent work such as \cite{Alaya2015}.
\end{itemize}

\subsubsection{Related work}
\label{subs:related_work}
Previous work has been done to survey the convergence between the \gls{iot} and the semantic web: 
\begin{itemize}
    \item Early work in the \gls{swot} focused on semantic sensor networks. For instance, \cite{Compton2009} surveys sensor ontologies and observation representations. 
    The scope of this work is especially on models, and even if it proposes an overview of technologies enabling semantic sensor networks, it does not present specific applications. 
    Similarly, \cite{Szilagyi2016} gives an overview of the semantic web stack applied to the \gls{iot}, and surveys \gls{iot} ontologies. 
    It goes beyond semantic sensor networks, but is still limited to models analysis. 
    We propose in this paper to focus on how the ontologies and the technologies of the semantic web are used to develop the \gls{swot}, rather than on identifying exhaustively the models used.
    \item \cite{Atzori2010} is a survey of the \gls{iot} domain, proposing a definition for the notion of \gls{iot} and listing application domains and enabling technologies for the \gls{iot}. 
    The \gls{iot} paradigm is described as the convergence of Internet technologies, electronic devices, and semantic web technologies. 
    However, the paper itself does not cover how semantic web technologies are integrated into the \gls{wot}, while we intend to analyze in detail and compare different contributions to the \gls{swot}.
    \item \cite{PayamBarnaghi117} studies the roles semantic web technologies can play in the \gls{iot}, as well as the challenges they represent. 
    This paper identifies some functions similar to what is presented in this paper in section \ref{sec:contributions}. 
    %However, we situated the contributions to these processes within \gls{lmu} in order to enable a deeper understanding of how an \gls{iot} network is integrated into the \gls{swot}, and to give a finer-grained analysis grid: we want to identify the impact of the nodes on the processes they are involved into. 
    However, the deployments of these functions in a reference architecture is not studied, and the technological constraints of the \gls{iot} are only described shallowly.
    \item \cite{Jara2014} gives an overview of the evolution from the \gls{iot} to the \gls{wot} and toward the \gls{swot}. 
    It is focused on the role of standards in interoperability, and the integration of semantic web technologies in standards. 
    It also provides an overview of technologies at stake in the \gls{iot}. 
    This paper is oriented toward projects and industrial consortiums, which is out of the scope of our study. 
    We focus on the contributions of the \gls{swot} to \gls{iot} issues,  and only integrate standardization concerns when they are related to this domain. 
    \item \cite{Sezer2018} gives a detailed overview of context-awareness in the \gls{iot}, and on the methodologies used to achieve it. 
    The authors focus more on functionalities than on deployments, but give a detailed analysis of the relationship between the \gls{iot}, big Data and machine learning, which is out of the scope of the present survey.
\end{itemize}  
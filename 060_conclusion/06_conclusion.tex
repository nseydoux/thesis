\chapter{Conclusion and future work}
\label{chap:conclusion}
\minitoc

\section{Conclusion}

\gls{iot} devices are deployed pervasively in the environment to provide services to human users.
To enforce compliance with principles of user-centric design, we determined the necessity for \textbf{interoperability among \gls{iot} devices}, and for the capacity of self-management of \gls{iot} systems.
From this observation, the emergence of the \gls{swot} domain is the logical culmination of the co-evolution of the \gls{iot} and \gls{sw} domains.
\gls{sw} technologies and principles were defined in the first place to represent machine-understandable knowledge, used to provide \gls{m2m} interoperability.
Achieving such interoperability is a necessity for the expansion of \gls{iot} networks, since they are composed of deeply heterogeneous devices required to communicate with each other.
However, the dynamism of \gls{iot} networks, and the inherent constraints of \gls{iot} devices represent challenges for the deployment of \gls{sw} technologies in the \gls{iot} context.
The work we propose in the present thesis addresses this twofold issue, in order to \textbf{achieve interoperability for \gls{iot} devices while adapting \gls{swot} technologies to \gls{iot} constraints}.

First, we described how interoperability, and especially semantic interoperability, is achieved in the \gls{swot}.
Some industrial standards are moving towards semantic interoperability, but the main tools to achieve it are ontologies.
In order to discuss the role of \gls{iot} ontologies as interoperability providers, two contributions have been described: 
\begin{itemize}
	\item We proposed ontology design requirements to characterize quality features for \gls{iot} ontologies, in order to support the adoption of good practices in the \gls{swot} community.
	Existing \gls{iot} ontologies have been identified, described, and assessed based on these requirements.
	Since existing ontologies did not fulfill the proposed design requirements, we instantiated them by defining IoT-O, a modular core-domain \gls{iot} ontology.
	\textbf{Contribution I.A of this thesis is the association of the \gls{iot} ontology design requirements and their instantiation by IoT-O.}
	To ensure a maximum interoperability, some IoT-O modules are based on existing ontologies, and IoT-O has been aligned to reference ontologies of the domain created after its publication.
	The description of IoT-O has been published in \cite{Seydoux2016}.
	\item IoT-O has been used as a semantic interoperability enabler in three use cases. 
	The \gls{opa} platforms proposes an open data which is updated daily with observations collected in a smart building. 
	These observations are enriched with knowledge described using extensions of IoT-O.
	The enriched observations are then transformed to be published in the FIESTA-IoT platform, a federated data hub deployed in a European project.
	The transformation making \gls{opa} observations compliant with the FIESTA-IoT platform are based on alignments between IoT-O and the FIESTA-IoT vocabulary.
	Finally, the role of \textbf{IoT-O and its extensions in driving the behavior of semIoTics, constituting contribution I.B of this thesis}, has been discussed.
	SemIoTics is an autonomic computing software used in a home automation use case, where syntactic and semantic interoperability are leveraged.
	SemIoTics was initially described in \cite{Seydoux2016_swit} and \cite{Aissaoui2016}.
\end{itemize}
Autonomic computing is a way to achieve self-managing \gls{iot} systems, but it requires constrained devices, in particular actuators, to adapt their behavior based on high-level decisions.
We proposed initial work for supporting semantic interoperability towards constrained devices with the mapping-reversal approach, published in \cite{Seydoux2016_poster}.
Such approach underlines the necessity to locate demanding computation on nodes with sufficient computing capabilities.

Indeed, \gls{sw} technologies tend to be resource-consuming, which is not suitable for an \gls{iot} deployment built upon constrained devices. 
Therefore, the \gls{sw} stack supporting \gls{swot} applications has often been deployed on Cloud nodes, leading to a centralization of the computation.
Such an approach impacts the scalability negatively, and degrades the \gls{qos}.
That is why we considered the Fog computing paradigm to enable the distribution of processing, and allow scalable deployments.
The technologically heterogeneous, and spatially spread nature of \gls{iot} systems requires the existence of a middle layer between \gls{iot} devices that collect data, and Cloud servers that process it.
This layer can be used for Fog computing, and this thesis surveys the role of semantic Fog computing in \gls{swot} architectures.

Surveying how semantic-enabled Fog nodes support the deployment of \gls{swot} systems led to the consideration of the complementarity of Cloud and Fog computing.
We proposed two contributions to leverage these paradigms:
\begin{itemize}
	\item \textbf{\gls{edr}, a generic approach for dynamically distributed rule-based reasoning} in \gls{swot} architectures has been introduced as \textbf{Contribution II.A}. 
	\gls{edr} defines a propagation technique for rules and data, driven by a deployment strategy.
	Applicative-level requirements for rule propagation are captured by the deployment strategy which is embedded into the rules.
	The core rule and data deployment technique is independent of the applicative-level requirements, making \gls{edr} generic.
	A description of \gls{edr} has been published in \cite{Seydoux2018_coopis}.
	\item \textbf{A refinement of \gls{edr}, called \edrt}, has then been described as \textbf{contribution II.B}. 
	\edrt implements a deployment strategy designed to reduce deduction delivery delay by bringing rule computation as close as possible to devices producing data.
	Two implementations of \edrptspace have been evaluated in two use cases, a smart building and a smart factory.
	The results of these experimentations supported our claim that decentralization improves scalability, and therefore supports the deployment of the \gls{swot}. 
	\edrt was initially incepted in \cite{Seydoux2018_itl}, and its detailed description was provided in \cite{Seydoux2018_wi}.
\end{itemize}

The purpose of the work presented in this thesis has been to support the development of semantic interoperability solutions in the domain of the \gls{iot} by promoting the emergence of \gls{swot} technologies adapted to \gls{iot} constraints.
By studying how \gls{sw} principles and technologies provide interoperability, and by considering a reasoning approach both scalable and dynamically distributed on semantic-aware Fog nodes, our contributions addressed these issues and supported further developments of the \gls{swot} domain. 
However, many challenges remain open in the convergence of \gls{sw} technologies and \gls{iot} devices, and they motivate our propositions of future work to overcome limitations identified in our own contributions.

\section{Future work}

\subsection{Short-term work: Extending the EDR ecosystem}
\label{subs:perspectives_edr}

\gls{edr} is a generic approach, supported by a vocabulary and refined by implementations of deployment strategies.
The core characteristics of \gls{edr} have been studied in this thesis, but further extensions can be considered.
These extensions aim at capturing different applicative requirements with new deployment strategies, but also to support the work of rule implementors.

\subsubsection{Potential deployment strategies}
\edrptspace is one possible refinement of \gls{edr}, focusing on improving response time by considering observation types.
In the remainder of this section, we list other parameters that may be considered to implement new deployment strategies. 

\paragraph{Considering node capabilities:}

The limitations of \edrt were discussed in Chapter \textsection \ref{chap:experimentations}, in particular its failure to consider the capabilities of Fog nodes in the rule deployment process.
Due to the heterogeneity of Fog nodes, it is naive for a given node to consider all its neighbors equally.
We intend to extend \edrptspace in order to make nodes aware of each other's capabilities when propagating rules to neighbors.
A challenge for this approach is that it should not be limited to the static characteristics of the nodes, such as memory or computing power.
It should also be adaptative to a node's dynamic state, for instance considering the battery state, or the rules that have already been forwarded to this node.

The heterogeneity of Fog nodes regarding communication capabilities is also a factor to consider. 
Depending on the deployed technologies, direct communication between Fog and Cloud nodes or applications might not be possible due to technical interoperability concerns. 
In such a topology, the \acrlong{adp} and \acrlong{cdp} delivery mechanisms cannot be applied, having this direct communication as a prerequisite.
In deployments where nodes communicate over ad-hoc networks, we discussed the role of border gateways as relays between the \gls{iot} and the Web.
Proposing a new approach derived from our \acrlong{cip} mechanism by identifying the critical gateways that ensure technical interoperability, will allow the relaxation of our hypothesis requiring direct communication between Fog nodes and application.

\paragraph{Domain identification:}

In the second version of the S-LOR, platform introduced in \cite{Gyrard2017}, rules are classified into domains.
It is possible to represent such classification as a taxonomy, in order to capture the domains into a \gls{kb}.
Once represented in a \gls{kb}, it is possible to use these domains to drive the propagation of rules among Fog nodes, by associating nodes and rules to domains.
We draw a prospective outline of such a deployment strategy, whose implementation by an \gls{edr} refinement could be referred to as \edrd.

Let us briefly consider \edrd though a smart campus example. 
The campus is composed of a library, a food court, student housing, and classroom buildings.
Each of these buildings belongs to a different domain, and therefore rules considering the same type of input (\textit{e.g.}, a temperature threshold, for simplicity) will not be executed equally in these different domains.
One could expect the desirable temperature to be warmer in an apartment than in a classroom for instance.
Instead of propagating all the rules measuring a temperature threshold to a node where temperature observations are available, only the rules associated to the node's domain should be considered. 
Moreover, one node may belong to several domains, and for instance, all places where fire hazards should be monitored can be classified in a ``Fire'' domain.

The core mechanism of \gls{edr} would be unchanged by such a deployment strategy: instead of proxying production types, nodes would proxy the domain.
This approach would implement a specialization logic, where depending on their context (here the domain), similar observation types are not processed with the same rules.
Moreover, it would make it possible to dynamically adapt the context in the case of mobile nodes.
For instance, let us consider a situation where a student with reduced mobility having a Fog node embedded in his/her wheelchair.
Such a node may be associated with the ``Reduced mobility'' domain, triggering dedicated rules in the different contexts it will appear.
The mobility of the node means that accessibility measures (automated doors, support from staff) are only set up when the student is present in the building, and supporting proper assistance whenever needed.

\paragraph{Privacy awareness:}

Privacy is a critical concern for the \gls{iot} R\&D community\footnote{\url{https://www.slideshare.net/kartben/iot-developer-survey-2018}}, as well as for \gls{iot} devices' end users. 
The recent multiplication of security breaches found in \gls{iot} systems\footnote{\url{https://internethealthreport.org/2018/spotlight-securing-the-internet-of-things/}} demonstrates the legitimacy of this concern.
Shifting the paradigm from the concentration of data in remote, centralized, third-party nodes to the propagation of processing close to data producers and consumers enforces the locality of data processing.
Enforcing privacy in \gls{iot} systems remains an open issue \cite{Miorandi2012}, despite some initial work for knowledge access control in the \gls{swot} \cite{Alam2010a}.

A refinement of \gls{edr} implementing a \textbf{privacy-aware deployment strategy} would therefore be an interesting prospective approach, that we refer to as \edrp
Such an approach would measure how the overhead of traffic, compared to the solution proposed in the present thesis, impacts performances, and find a trade-off between privacy and performances.
\edrp would rely on a partially ordered credentials definition: each data producer (typically a sensor) would be given a credential level, attached to each observation it produces, and each node would also be attributed with a credential level, inspired by \cite{Singh2017} or Unix user groups.
The credential level of a node is typically a characteristic that would be declared by a node to its neighbors, based on the \gls{edr} announcement mechanism.
A node may be forwarded a piece of data if and only if this node has a credential level that is both comparable and superior or equal to the credential of the piece of data.
The credential level of a child is considered superior to that of its ancestors: the notion of context imbrication on which \edrptspace is based can also be adapted to privacy.
For sibling nodes, and \textit{a fortiori} in the general case, credentials are not comparable, unless explicitly stated so.

A specificity of \edrp compared to \edrptspace is that it is possible that an observation is not only propagated upward.
Indeed, the notion of lowest common ancestor as it is used in \edrptspace assumes that any node may access any information, which is no longer the case in \edrp.
Therefore, the rule propagation module of an \edrp rule should not only check if a child node is a potential candidate for rule application, but also declare the child node consumer of observations collected by its parent, which is contrary to the \edrptspace logic.

The notion of policy as defined in \cite{Singh2017} should also be considered when attributing a confidentiality level to a deduction. 
Since the different elements considered by the rule might be of varying confidentiality, determining the confidentiality of the rule result is a non-trivial issue, necessitating an explicit policy expressed as part of the rule.
Simple examples of policy include ``most restrictive'' or ``least restrictive'', where the confidentiality of the result is directly inherited from the confidentiality of one of the rule's input, but more complex aggregation policies may be implemented as well.

\subsubsection{Supporting rule management}

\paragraph{Rule hosting platform:} After considering improvements in the rule deployment process itself, we intend to improve the management of rules as well.
The modularity of \gls{edr} rules does not facilitate their development, and requires some expertise for rule implementors. 
In order to improve the accessibility of rules, a sharing platform inspired by \cite{Gyrard2017} is also considered as a future work.
Since \gls{edr} rules are compliant with Linked Open Rules principles, the platform may offer access to rules presented as Web resources, improving their reusability. 
Moreover, having a central repository for rules enables the approach to consider a reference version of the rule, as the dereferenced resource hosted by said repository. 
Since rules are identified by \gls{iri}, it is possible to incrementally modify them at runtime, so that the operation of the controlled system is not interrupted. 
Modifying rules allow applications to fine-tune their behavior according to a feedback loop that considers either previous responses to inputs, or external factors (\textit{e.g.,} seasonal change, or regulation evolution).

\paragraph{Rule development support:} Developing rules embedding deployment strategies may be a challenging task. 
In order to support rule implementors in their development process, the hosting platform presented in the previous paragraph could include rule-building tools.
These tools will enable the construction of rules embedding existing deployment strategies such as \edrt, as well as the visualization of existing rules on the platform.

\paragraph{Multiple concurrent deployment strategies:}
So far, the deployment strategies implemented by refinements of \gls{edr} have been considered individually, with only one strategy driving rule deployment in a particular \gls{swot} network.
Since \gls{edr} is a generic approach agnostic to the deployment strategy, it is technically possible to deploy rules embedding multiple concurrent deployment strategies.
However, inconsistent behaviors may be induced by the execution of contradictory deployment strategies.
For instance, the decisions based on privacy considerations in \edrp might be undermined by decisions driven by efficiency in \edrt.
The identification of inconsistent strategies, and the mechanisms to prevent detrimental behaviors are challenges that should be addressed in future work.

\subsection{Medium-term work: Bringing semIoTics and EDR together}

In its current state, \gls{edr} feeds remote applications deductions, that these applications can use in a decision-making process.
Such decisions may be taken by human operators, in which case the application provides the operator with a representation of the deductions received from the network, implementing a presentation functionality as described in Section \textsection \ref{par:presentation}.
However, it is also possible that the application reacts to the deductions by taking action without a direct human intervention.
In this case, the model of autonomic computing, already introduced in Section \textsection \ref{subsubs:mape_k} when discussing a home automation use case, may be applied.

Let us consider the application featured in the use case described in Section \textsection \ref{sec:distribution_use_case} as an autonomic agent implementing a MAPE-K loop.
When applying \gls{edr}, the implementation of the MAPE-K loop is incomplete: the Monitoring and Analysis steps are indeed performed, by collecting observations and processing them with rules, but the Planning and Execution steps are not discussed.

The \gls{edr} approach aims at bursting the MAPE-K loop implemented by the application: instead of receiving all the raw observations (Monitoring) and making the deductions itself (Analysis), the application spreads its Analysis rules in the network so that both Monitoring and Analysis are distributed.
In order to achieve a full distribution of the MAPE-K loop, it would be necessary to decompose the Planning modules into rules as well, in order to make decisions locally based on the deductions inferred from observations, as well as enabling the local implementation of the Execution step.
In order to enable the transformation of high-level action representations, issued from the Planning step, into messages understandable by legacy devices, the mapping reversal approach introduced in Section \textsection \ref{sec:mapping_reversal} should be integrated into the Execution step.

A complex system may be composed of several autonomic elements, each of them implementing its own MAPE-K loop. 
Therefore, the decentralization of the application would be complete, with MAPE-K loop instances deployed opportunistically in the network in order to control devices at a local scale.
This approach would lead to a multiscale system-of-systems \gls{iot} automation and self-configuration, by enabling the creation of interworked autonomous systems depending on each other.
Multiscale modelling is discussed in \cite{Gassara2017}, and the concept of system-of-systems is defined in \cite{Boardman2006}.
Generalizing autonomic computing at different levels of \gls{iot} deployments would enable a full-stack semantic interoperability, from a high-level policy expressed to drive an \gls{iot} to atomic device behavior implementing such strategy.
\gls{iot} networks are then treated as holons \cite{Koestler1967}, that is to say that they are both a whole and a part, depending on the granularity of the policy considered. 
As a whole, they have a purpose and they can be considered an atomic system, and, as a part, they contribute to the purpose of the system at a larger scale \cite{Oliveira2013}.
This approach leads to the emergence of \textbf{local behaviors enforcing global policies}, which is a more scalable pattern bridging high-level, global user requirements to low-level, local actions.

\subsection{Long-term work: Supporting natural-language interaction}

User-centric design is a global approach that motivates the contributions of my work.
How user requirements are collected in the first place is an actively discussed topic in this approach.
The fundamental way of human communication is natural language, and usually user-centered design is driven by the interaction between designers and users.
Moreover, in this work we considered systems in which user requirements are not only considered at design time, but are also driving the behavior of the system.
For \gls{swot} systems, user preferences are usually expressed through a \gls{gui}, as in \cite{Kaed2018} or \cite{Kasnesis2015}.
However, such expressiveness is limited to the use cases designed by the implementor of the \gls{gui}, restricting the customization for the user.
The expression of requirements in natural language is a way to enable a more user-friendly interaction.

An issue with natural language interaction in the \gls{iot} is the mostly numerical nature of the exchanged data.
However, the emergence of the \gls{swot} introduces natural language resources in data annotation. 
Such resources enable systems to interact with the user not only via graphical interfaces, but also through conversational interfaces. 
We presented preliminary work for query-answering dedicated to \gls{iot} systems in \cite{IC2017}.
The current popularity of vocal assistants, the ever growing capabilities of speech recognition for smart phones, and open projects fostering the production of large corpora of voice recordings\footnote{\url{https://voice.mozilla.org/}}, combined to the development of chatbots, denotes the importance of conversational interfaces facing end users. 
Not only are direct commands and interactions enabled by such interfaces (``Switch on the bedroom reading light''), but high-level policies driving autonomic behavior can also be specified by the user.
A natural language interface would therefore be a user-centered endpoint deployed on top of a joined semIoTics/\gls{edr} system.\newline

Mark Weiser, a major figure of the pervasive computing paradigm, stated that: ``The most profound technologies are those that disappear. They weave themselves into the fabric of everyday life until they are indistinguishable from it'' \cite{Weiser2002}. 
Enabling through the \gls{swot} conversational interactions with complex systems integrated into our environments while ensuring privacy and dynamic adaptability, is achieving a vision of the \gls{iot} where the technology does indeed disappear, to leave humans in a ``smarter'' environment.

# Distribution
- Single host:
  - /home/nseydoux/dev/edr/results/thesis/iter_5/boxplot-distrib.py
  - /home/nseydoux/dev/edr/results/thesis/iter_5/stacked_bars_distrib.py
- Multi host: /home/nseydoux/dev/edr/results/thesis/iter_7/boxplot-distrib.py
  -

# Scalability
- Single host: /home/nseydoux/dev/edr/results/thesis/iter_11/boxplot-scala.py
- Multi host: /home/nseydoux/dev/edr/results/thesis/iter_7/boxplot-scala.py
- RPi2 : /home/nseydoux/dev/edr/results/thesis/iter_6_rpi2/boxplot-scala.py
